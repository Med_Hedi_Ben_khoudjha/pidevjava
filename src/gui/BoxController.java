/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity_ib.Produit;
//import Entity.Programme;
import Service.ServiceProduit;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import ibtih.MyConnection;
import ibtih.Nawara;

/**
 * FXML Controller class
 *
 * @author user
 */
public class BoxController implements Initializable {
    

    ListView<Produit> list = new ListView<>();
    ObservableList<Produit> items = FXCollections.observableArrayList();
    private ObservableList<Produit> data;
    Connection cn = MyConnection.getInstance().getConnection();
    
    Produit current_p = new Produit();

    @FXML
    private AnchorPane ap;
    @FXML
    private Pane sq;
    @FXML
    private Label txtitre;
    @FXML
    private Label quantite;
    @FXML
    private Label id;
    @FXML
    private Rectangle rectangle;
    @FXML
    private Button shwprod;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        ServiceProduit ps = new ServiceProduit();
        //List<Produit> data = ps.getAll();
        List<Produit> data = ps.getAllBySousCategorie();
        
        for (Produit p : data) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/DetailsProduct.fxml"));
            txtitre.setText(p.getNomProduit());
            
            Image im = new Image("http://localhost/pidevweb/web/img/" + p.getPhotoProduit());
            rectangle.setFill(new ImagePattern(im));

        }

        /*    for(Produit p : dataSCat){
            //FXMLLoader loader = new FXMLLoader(getClass().getResource("DetailsProduct.fxml"));
            txtitre.setText(p.getNomProduit());
            Image im = new Image("http://localhost/dotcom_2019/web/img/"+p.getPhotoProduit());
            rectangle.setFill(new ImagePattern(im));
       
          } */
    }
    
    
    public void LoadValues(Produit e) throws IOException {
        txtitre.setText(e.getNomProduit());
        Image im = new Image("http://localhost/pidevweb/web/img/" + e.getPhotoProduit());
        rectangle.setFill(new ImagePattern(im));
    }

    public ObservableList<Produit> loadProduit() throws SQLException {
        PreparedStatement pt = cn.prepareStatement("SELECT produit.nomProduit, produit.photoProduit   from produit ");
   
        ResultSet resultat = pt.executeQuery();
        while (resultat.next()) {
            String nom = resultat.getString("nomProduit");
            String img = resultat.getString("photoProduit");
            Produit p = new Produit(nom, img);
            items.add(p);
        }
        return items;
    }

    @FXML
    private void ShowProduct(MouseEvent event) throws SQLException {

        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/gui/DetailsProduct.fxml"));
        Produit prog = new Produit();
        try {
            Loader.load();
            
            
            ServiceProduit psp = new ServiceProduit();
            DetailsProductController aff = Loader.getController();

            prog = psp.getWithCat(txtitre.getText());
            Nawara.p = prog;
            System.out.println("momken temchi "  + Nawara.p.getSousCatg());
            aff.setProduit(prog);
            
            
            Parent p = Loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
            System.out.println(prog.getSousCatg());
        }

    }

    public Produit getCurrent_p() {
        return current_p;
    }

    public void setCurrent_p(Produit current_p) {
        this.current_p = current_p;
    }
    
    

}
