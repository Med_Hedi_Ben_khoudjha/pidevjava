/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity_ib.Reclamation;
import Entity_ib.Utilisateur;
import Service.ServiceProduit;
import Service.ServiceReclamation;
import Service.ServiceUser;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;


/**
 * FXML Controller class
 *
 * @author Zaineb
 */
public class UtilisateurController implements Initializable {

    @FXML
    private TextField rechercher;
    @FXML
    private Button rech;
    @FXML
    private Button btnOverview;
    @FXML
    private Button btnCustomers;
    @FXML
    private Button btnMenus;
    @FXML
    private Button btnPackages;
    @FXML
    private Button btnOrders;
    @FXML
    private Button btnSettings;
    @FXML
    private Button btnSignout;
    @FXML
    private Pane pnlCustomer;
    @FXML
    private Pane pnlOrders;
    @FXML
    private Pane pnlMenus;
    @FXML
    private Pane pnlOverview;
     @FXML
    private Button ajouter;
      @FXML
    private Button modifier;
       @FXML
    private Button afficher;
        @FXML
    private Button supprimer;
      @FXML
    private Button adduserbutton;
       @FXML
    private Button annuler;
     @FXML
    private TabPane tab;
     @FXML
    private TableView<Utilisateur> utilisateur;
    @FXML
    private TableColumn<Utilisateur, String> id;
    @FXML
    private TableColumn<Utilisateur, String> nom;
    @FXML
    private TableColumn<Utilisateur, Date> prenom;
    @FXML
    private TableColumn<Utilisateur, String> adresse;
    @FXML
    private TableColumn<Utilisateur, String> numTel;
    @FXML
    private TableColumn<Utilisateur, String> email;
    @FXML
    private TextField addnom;
    @FXML
    private TextField addprenom;
    @FXML
    private TextField addadresse;
    @FXML
    private TextField addemail;
    @FXML
    private TextField addnumTel;
     @FXML
    private TextField mnom;
    @FXML
    private TextField mprenom;
    @FXML
    private TextField madresse;
    @FXML
    private TextField memail;
    @FXML
    private TextField mnumTel;

    private ObservableList<Utilisateur> ListUtilisateur = FXCollections.observableArrayList();
    Utilisateur u = new Utilisateur(); 
     ServiceUser su = new ServiceUser();
     

   /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
         Afficher();
          supprimer.setVisible(false);
      modifier.setVisible(false);
       annuler.setVisible(false);
      afficher.setVisible(false);
    }  
   void Afficher()
    {
         
        ListUtilisateur = FXCollections.observableArrayList(su.getAll());
        System.out.println(ListUtilisateur);
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        id.cellFactoryProperty();
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        nom.cellFactoryProperty();
        prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        prenom.cellFactoryProperty();
        adresse.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        adresse.cellFactoryProperty(); 
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        email.cellFactoryProperty()  ;     
        numTel.setCellValueFactory(new PropertyValueFactory<>("numTel"));
        numTel.cellFactoryProperty();
        
         
           utilisateur.setItems(ListUtilisateur);
        
        
    }
   @FXML
   private void Enregistrer_Ajout(ActionEvent event)
   {
              
        Utilisateur u = new Utilisateur(addnom.getText(), addprenom.getText(),addadresse.getText(),addemail.getText(),addnumTel.getText()); 
       
       su.adduser(u);
        Afficher();
        addnom.setText("");
        addprenom.setText("");
        addadresse.setText("");
        addemail.setText("");
        addnumTel.setText("");
     
       tab.getSelectionModel().select(0);
    
   }
   
    @FXML
    private void afficher_formulaire_ajout(ActionEvent event)
    {
     
     tab.getSelectionModel().select(1);
    } 
    @FXML
    private void table_clicked(MouseEvent event)
    {
     
      supprimer.setVisible(true);
      modifier.setVisible(true);
      afficher.setVisible(true);
      annuler.setVisible(true);
//      ajouter.setVisible(false);
    }
    @FXML
     private void Annuler_selection(ActionEvent event)
    {
            supprimer.setVisible(false);
            modifier.setVisible(false);
            annuler.setVisible(false);
            afficher.setVisible(false);
            ajouter.setVisible(true);
     
    }
     @FXML
     private void supprimer_Modification(ActionEvent event)
     {
         if (!utilisateur.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Suppression d'un utilisateur");
            alert.setHeaderText("Etes-vous sur de vouloir supprimer l'utilisateur "
                    + utilisateur.getSelectionModel().getSelectedItem().getNom()+ "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ServiceUser su = new ServiceUser();
               
               su.deleteuser(utilisateur.getSelectionModel().getSelectedItem().getId());
              
                Afficher();
                
                
            }
            supprimer.setVisible(false);
            modifier.setVisible(false);
            annuler.setVisible(false);
            afficher.setVisible(false);
            ajouter.setVisible(true);
        }
     }
      @FXML
     private void afficher_formulaire_modifier(ActionEvent event)
     {
         
         mnom.setText(utilisateur.getSelectionModel().getSelectedItem().getNom());
         mprenom.setText(utilisateur.getSelectionModel().getSelectedItem().getPrenom());
         memail.setText(utilisateur.getSelectionModel().getSelectedItem().getEmail());
         madresse.setText(utilisateur.getSelectionModel().getSelectedItem().getAdresse());
         mnumTel.setText(utilisateur.getSelectionModel().getSelectedItem().getNumTel());
         //prenom.setText(utilisateur.getSelectionModel().getSelectedItem().getPrenom());
             
         
         
        // valider.setVisible(false);
        tab.getSelectionModel().select(2);
         supprimer.setVisible(false);
            modifier.setVisible(false);
            annuler.setVisible(false);
           // ajouter.setVisible(true);
     }
      @FXML
     private void Enregistrer_modification(ActionEvent event)
     { //int id= utilisateur.getSelectionModel().getSelectedItem().getId();
      if (!utilisateur.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("suppression d'une catégorie d'event");
            alert.setHeaderText("Etes-vous sur de vouloir modifier "
                    + utilisateur.getSelectionModel().getSelectedItem().getNom()+ "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {    
                ServiceUser us=new ServiceUser();
                Utilisateur user = new Utilisateur(utilisateur.getSelectionModel().getSelectedItem().getId(),mnom.getText(), mprenom.getText(),madresse.getText(),mnumTel.getText(),memail.getText()); 
                us.updateuser(user);
                
                System.out.println("___________a_________");
                System.out.println(id);
                System.out.println(user);
                System.out.println("___________a_________");
                
              
              
                Afficher();
                
                
            }
            
            mnom.setText("");
            mprenom.setText("");
            madresse.setText("");
            memail.setText("");
            mnumTel.setText("");
     
         afficher.setVisible(false);
         tab.getSelectionModel().select(0);
         
     }
     }
     
     
    

    @FXML
    private void handleClicks(ActionEvent event) {
    }
    
    
}
