/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity_ib.Produit;
import Entity_ib.SousCategorie;
import Service.ServiceProduit;
import Service.SousCategorieService;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.spi.CalendarDataProvider;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.print.attribute.standard.DateTimeAtCreation;
import ibtih.MyConnection;
import ibtihel.copyImages;
import pidev.main;
import sun.util.calendar.CalendarSystem;

/**
 * FXML Controller class
 *
 * @author dorra
 */
public class ProduitController implements Initializable {

    /**
     * Initializes the controller class.
     */
    Connection cn = MyConnection.getInstance().getConnection();
    @FXML
    private DatePicker dateProduit;

    @FXML
    private TableView<Produit> produit;
    @FXML
    private TableColumn<Produit, String> col_nom;
    @FXML
    private TableColumn<Produit, String> col_reference;
    @FXML
    private TableColumn<Produit, String> col_prixAchat;
    @FXML
    private TableColumn<Produit, String> col_prixVente;
    @FXML
    private TableColumn<Produit, String> col_quantite;
    //   @FXML
    // private TableColumn<Produit,String> col_dateProduit;
    //  private final TabPane tab = new TabPane();
    @FXML
    private TabPane tab;

    ObservableList<Produit> ListProduits = FXCollections.observableArrayList();
    ObservableList<String> ListSouscategories = FXCollections.observableArrayList();
    Produit r = new Produit();
    ServiceProduit Rs = new ServiceProduit();
    SousCategorieService scatgs = new SousCategorieService();
    SousCategorie scat = new SousCategorie();

    @FXML
    private Button btnOverview;
    @FXML
    private Button btnCustomers;
    @FXML
    private Button btnMenus;
    @FXML
    private Button btnPackages;
    @FXML
    private Button btnOrders;
    @FXML
    private Button btnSettings;
    @FXML
    private Button btnSignout;
    @FXML
    private Pane pnlCustomer;
    @FXML
    private Pane pnlOrders;
    @FXML
    private Pane pnlMenus;
    @FXML
    private Pane pnlOverview;
    @FXML
    private Pane pnlCustomer1;
    @FXML
    private Pane pnlOrders1;
    @FXML
    private Pane pnlMenus1;
    @FXML
    private Pane pnlOverview1;
    @FXML
    private TextField nom;
    @FXML
    private TextField reference;
    @FXML
    private TextField prixAchat;
    @FXML
    private TextField prixVente;
    @FXML
    private TextField quantite;
    @FXML
    private TextField photoProduit;
    @FXML
    private Button ajouterProd;
    @FXML
    private Button ajouterProduit;

    @FXML
    private Button btn_suppProd;
    @FXML
    private Tab formajout;

    @FXML
    private Button btn_modifierProd;
    @FXML
    private Tab formajout1;
    @FXML
    private Pane pnlCustomer11;
    @FXML
    private Pane pnlOrders11;
    @FXML
    private Pane pnlMenus11;
    @FXML
    private Pane pnlOverview11;
    @FXML
    private Button btn_modifier;
    @FXML
    private TextField nom1;
    @FXML
    private TextField prixAchat1;
    @FXML
    private TextField prixVente1;
    @FXML
    private TextField quantite1;
    @FXML
    private ComboBox<String> SousCat;
Stage dialogStage = new Stage();
    Scene scene;
    private String absolutePathPhoto;
    @FXML
    private Button PhotoUpload;
    @FXML
    private Button btn_afficherPhoto;
    @FXML
    private TextField search;
    @FXML
    private TableColumn<?, ?> col_date;
    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        SousCat.setItems(ListSouscategories);
        List<SousCategorie> myList = scatgs.getSousCategorie();
        List<String> myStrings = new ArrayList<>();
        for (SousCategorie s : myList) {
            myStrings.add(s.getNomSousCategorie());
        }

        SousCat.setItems(FXCollections.observableArrayList(myStrings));
        
          search.textProperty().addListener(new ChangeListener<String>() {
    @Override
    public void changed(ObservableValue<? extends String> observable,
            String oldValue, String newValue) {
       
       recherche();
    }

        } );

        Afficher();
        btn_suppProd.setVisible(true);
        // ajouterProd = new Button();
        //ajouterProd.setOnAction( e -> tab.getSelectionModel().select(1));

    }
    
     void recherche ()
{
 if(search.getText().equals(""))
     {
         Afficher();
     }
     else {
      ListProduits = FXCollections.observableArrayList(Rs.rechercherProduit(search.getText()));
      col_nom.setCellValueFactory(new PropertyValueFactory<>("nomProduit"));
        col_nom.cellFactoryProperty();
        col_quantite.setCellValueFactory(new PropertyValueFactory<>("quantite"));
        col_quantite.cellFactoryProperty();

           produit.setItems(ListProduits);
     }
}
    
   @FXML
    private void afficher_LesCategories(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Categorie.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
    @FXML
    private void afficher_LesSousCategories(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/SousCategorie.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
    @FXML
    private void afficher_Produits(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Produit.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
    
    
    
    
    
    
    
    
    
    
    
    @FXML
     private void photoChooser(ActionEvent event)
     {
          FileChooser fileChooser = new FileChooser();
         fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg")
         );
     
            File choix = fileChooser.showOpenDialog(null);
            if (choix != null) {
              
                absolutePathPhoto = choix.getAbsolutePath();
                photoProduit.setText(choix.getName());
             } else {
                System.out.println("Image introuvable");
            }
        
     }
    
    
    
    
    

    void Afficher() {

        ListProduits = FXCollections.observableArrayList(Rs.getAll());

      
        col_nom.setCellValueFactory(new PropertyValueFactory<>("nomProduit"));
        col_nom.cellFactoryProperty();

        col_reference.setCellValueFactory(new PropertyValueFactory<>("reference"));
        col_reference.cellFactoryProperty();

        col_prixAchat.setCellValueFactory(new PropertyValueFactory<>("prixAchat"));
        col_prixAchat.cellFactoryProperty();

        col_prixVente.setCellValueFactory(new PropertyValueFactory<>("prixVente"));
        col_prixVente.cellFactoryProperty();

        col_quantite.setCellValueFactory(new PropertyValueFactory<>("quantite"));
        col_quantite.cellFactoryProperty();
        
        
        col_date.setCellValueFactory(new PropertyValueFactory<>("dateProduit"));
        col_date.cellFactoryProperty();
        
        
        

       

        // col_dateProduit.setCellValueFactory(new PropertyValueFactory<>("dateProduit"));
        // col_dateProduit.cellFactoryProperty();
        produit.setItems(ListProduits);

    }

    @FXML
    private void afficher_formulaire_ajouter(ActionEvent event) {
        tab.getSelectionModel().select(1);
        // trying.setVisible(true);

    }
    
    @FXML
    private void retourneProduits(ActionEvent event) {
        tab.getSelectionModel().select(0);
        // trying.setVisible(true);

    }

    @FXML
    private void Enregistrer_AjoutProduit(ActionEvent event) {
        
        
    LocalDate date = LocalDate.now(); 
   // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
 
        
        SousCategorie s = scatgs.getSousCategorieByName(SousCat.getSelectionModel().getSelectedItem());
         copyImages.deplacerVers(photoProduit, absolutePathPhoto,"C:\\Users\\MED\\Desktop\\esprit\\3eme\\med\\src\\images");
         copyImages.deplacerVers(photoProduit, absolutePathPhoto,"C:\\wamp64\\www\\pidevweb\\web\\img");
        Produit p = new Produit(nom.getText(), reference.getText(), Float.parseFloat(prixVente.getText()), Float.parseFloat(prixAchat.getText()), Integer.parseInt(quantite.getText()), photoProduit.getText(), String.valueOf(dateProduit.getValue()), s.getId());

            if (nom.getText().equals("") || reference.getText().equals("") || prixVente.getText()==null || prixAchat.getText()==null || quantite.getText()==null || photoProduit.getText().equals("")  ) { 
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Alerte");
            alert.setHeaderText("Vous devez remplir TOUT LES CHAMPS SVP");
             Optional<ButtonType> result = alert.showAndWait();
             if (result.get() == ButtonType.OK) {
             tab.getSelectionModel().select(1);
              Afficher();
        nom.setText("");
        reference.setText("");
        prixVente.setText("");
        prixAchat.setText("");
        quantite.setText("");       
        quantite.setText("");
        
             }
            
            }
            else if ( !dateProduit.getValue().equals(date))
            {    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Alerte");
            alert.setHeaderText("Date erronée ");
             Optional<ButtonType> result = alert.showAndWait();
             if (result.get() == ButtonType.OK) {
             tab.getSelectionModel().select(1);
              Afficher(); 
               dateProduit.setValue(null);}}

else {  Rs.ajouterproduit(p);
        Afficher();
        nom.setText("");
        reference.setText("");
        prixVente.setText("");
        prixAchat.setText("");
        quantite.setText("");
        photoProduit.setText("");
        quantite.setText("");
        dateProduit.setValue(null);
        tab.getSelectionModel().select(0);}

    }

    @FXML
    private void supprimer_Modification(ActionEvent event) {
        if (!produit.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Suppression d'un produit");
            alert.setHeaderText("Etes-vous sur de vouloir supprimer le produit "
                    + produit.getSelectionModel().getSelectedItem().getNomProduit() + "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ServiceProduit su = new ServiceProduit();
                System.out.println(produit.getSelectionModel().getSelectedItem().getId());
                su.supprimerProduit(produit.getSelectionModel().getSelectedItem().getId());

                Afficher();

            }
            btn_suppProd.setVisible(true);
            // modifier.setVisible(false);
            //annuler.setVisible(false);
            //afficher.setVisible(false);
            //ajouter.setVisible(true);*/
        }
    }

    @FXML
    private void afficher_formulaire_modifier(ActionEvent event) {

        nom1.setText(produit.getSelectionModel().getSelectedItem().getNomProduit());
        prixAchat1.setText(Float.toString(produit.getSelectionModel().getSelectedItem().getPrixAchat()));
        prixVente1.setText(Float.toString(produit.getSelectionModel().getSelectedItem().getPrixVente()));
        quantite1.setText(Integer.toString(produit.getSelectionModel().getSelectedItem().getQuantite()));
        tab.getSelectionModel().select(2);
        //prenom.setText(utilisateur.getSelectionModel().getSelectedItem().getPrenom());

        // valider.setVisible(false);
        btn_suppProd.setVisible(true);
        //  modifier.setVisible(false);
        //annuler.setVisible(false);
        // ajouter.setVisible(true);
    }

    @FXML
    private void Enregistrer_modification(ActionEvent event) {
        if (!produit.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Modification d'un produit");
            alert.setHeaderText("Etes-vous sur de vouloir modifier "
                    + produit.getSelectionModel().getSelectedItem().getNomProduit() + "?");
            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {

                ServiceProduit us = new ServiceProduit();
                Produit p = new Produit(produit.getSelectionModel().getSelectedItem().getId(), nom1.getText(), Float.parseFloat(prixVente1.getText()), Float.parseFloat(prixAchat1.getText()), Integer.parseInt(quantite1.getText()));
                us.modifierProduit(p);
                System.out.println(p);
                System.out.println("___________a_________");

                Afficher();

            }

            nom1.setText("");
            prixVente1.setText("");
            prixAchat1.setText("");
            quantite1.setText("");

            //afficher.setVisible(false);
            tab.getSelectionModel().select(0);

        }
    }
    
    @FXML
    private void ShowPhoto(ActionEvent event) throws SQLException {
         
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/gui/PhotoProduit.fxml"));
        
        try {
            Loader.load();
            Produit prog = new Produit() ;     
            ServiceProduit psp = new ServiceProduit();
            PhotoProduitController aff= Loader.getController();
        
            prog= psp.getPhotos(produit.getSelectionModel().getSelectedItem().getNomProduit());
            
            aff.setProduit(prog);
            Parent p = Loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
            } catch (IOException ex) {
                System.out.println(ex);
            }        
            
    }
    
    
     @FXML
    private void ShowBest(ActionEvent event) throws SQLException {
         
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("BestProduct.fxml"));
        
        try {
            Loader.load();
            Produit prog = new Produit() ;     
            ServiceProduit psp = new ServiceProduit();
            BestProductController aff= Loader.getController();
        psp.getBest();
            aff.loadProduit();
            Parent p = Loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
            } catch (IOException ex) {
                System.out.println(ex);
            }        
            
    }

    @FXML
    private void quit(MouseEvent event) throws IOException {
        
        Parent root = FXMLLoader.load(getClass().getResource("/pidev/login.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void home(MouseEvent event) throws IOException {
         Parent root = FXMLLoader.load(getClass().getResource("/pidev/acc.fxml"));
        main.stage.setScene(new Scene(root));
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
}
