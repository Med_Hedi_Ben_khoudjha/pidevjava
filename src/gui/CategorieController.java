/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity_ib.Categorie;
import Entity_ib.SousCategorie;
import Service.ServiceCategorie;
import Service.SousCategorieService;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pidev.main;

/**
 * FXML Controller class
 *
 * @author dorra
 */
public class CategorieController implements Initializable {

    @FXML
    private TabPane tab;
    @FXML
    private Pane pnlCustomer;
    @FXML
    private Pane pnlOrders;
    @FXML
    private Pane pnlMenus;
    @FXML
    private Pane pnlOverview;
    @FXML
    private TableView<Categorie> Categorie;
    @FXML
    private TableColumn<Categorie, String> nom_cat;
    @FXML
    private TableColumn<Categorie, String> reference_cat;
    @FXML
    private Button btn_suppCat;
    @FXML
    private Button btn_modifierCat;
    @FXML
    private Tab formajout;
    @FXML
    private Button btnOverview1;
    @FXML
    private Button btnCustomers1;
    @FXML
    private Button btnMenus1;
    @FXML
    private Button btnPackages1;
    @FXML
    private Button btnOrders1;
    @FXML
    private Button btnSettings1;
    @FXML
    private Button btnSignout1;
    @FXML
    private Pane pnlCustomer1;
    @FXML
    private Pane pnlOrders1;
    @FXML
    private Pane pnlMenus1;
    @FXML
    private Pane pnlOverview1;
    @FXML
    private Button ajouterCategorie;
    @FXML
    private TextField nomCat;
    @FXML
    private TextField referenceCat;
    @FXML
    private Tab formajout1;
    @FXML
    private Pane pnlCustomer11;
    @FXML
    private Pane pnlOrders11;
    @FXML
    private Pane pnlMenus11;
    @FXML
    private Pane pnlOverview11;
    @FXML
    private TextField nomCat1;
    @FXML
    private TextField referenceCat1;
    ServiceCategorie catgs = new ServiceCategorie();
    Categorie cat = new Categorie();
    @FXML
    private Button ajouterCat;
    Stage dialogStage = new Stage();
    Scene scene;

 ObservableList<Categorie> Listcategories = FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Afficher();
    }   
    
     void Afficher() {

        Listcategories = FXCollections.observableArrayList(catgs.getAll());
         
      
        
        
        nom_cat.setCellValueFactory(new PropertyValueFactory<>("nomCategorie"));
        nom_cat.cellFactoryProperty();

        reference_cat.setCellValueFactory(new PropertyValueFactory<>("referenceCategorie"));
        reference_cat.cellFactoryProperty();

       

        // col_dateProduit.setCellValueFactory(new PropertyValueFactory<>("dateProduit"));
        // col_dateProduit.cellFactoryProperty();
        Categorie.setItems(Listcategories);

    }
    
    
    
    
    
      @FXML
    private void afficher_formulaire_ajouter(ActionEvent event) {
        tab.getSelectionModel().select(1);
        
        // trying.setVisible(true);

    }
    
     @FXML
    private void Enregistrer_AjoutCAT(ActionEvent event) {
        
       
        Categorie p = new Categorie(nomCat.getText(), referenceCat.getText());
  if (nomCat.getText().equals("") || referenceCat.getText().equals("") ) { 
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Alerte");
            alert.setHeaderText("Vous devez remplir TOUT LES CHAMPS SVP");
             Optional<ButtonType> result = alert.showAndWait();
             if (result.get() == ButtonType.OK) {
             tab.getSelectionModel().select(1);
              Afficher();
        nomCat.setText("");
        referenceCat.setText("");}}
  
  else {
        
        catgs.ajoutercategorie(p);
        Afficher();
        nomCat.setText("");
        referenceCat.setText("");
        
        tab.getSelectionModel().select(0);}

    }
    
    
     @FXML
    private void supprimer(ActionEvent event) {
        if (!Categorie.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Suppression d'une Sous Categorie");
            alert.setHeaderText("Etes-vous sur de vouloir supprimer la sous categorie "
                    + Categorie.getSelectionModel().getSelectedItem().getNomCategorie()+ "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ServiceCategorie su = new ServiceCategorie();
                System.out.println(Categorie.getSelectionModel().getSelectedItem().getId());
                su.supprimerCat(Categorie.getSelectionModel().getSelectedItem().getId());

                Afficher();

            }
            btn_suppCat.setVisible(true);
            // modifier.setVisible(false);
            //annuler.setVisible(false);
            //afficher.setVisible(false);
            //ajouter.setVisible(true);*/
        }
    }
    
    
    
     @FXML
    private void afficher_formulaire_modifier(ActionEvent event) {

        nomCat1.setText(Categorie.getSelectionModel().getSelectedItem().getNomCategorie());
        referenceCat1.setText(Categorie.getSelectionModel().getSelectedItem().getReferenceCategorie());
       
        tab.getSelectionModel().select(2);
        //prenom.setText(utilisateur.getSelectionModel().getSelectedItem().getPrenom());

        // valider.setVisible(false);
       // btn_suppProd.setVisible(false);
        //  modifier.setVisible(false);
        //annuler.setVisible(false);
        // ajouter.setVisible(true);
    }

    @FXML
    private void Enregistrer_modification(ActionEvent event) {
        if (!Categorie.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("suppression d'une sous categorie");
            alert.setHeaderText("Etes-vous sur de vouloir modifier "
                    + Categorie.getSelectionModel().getSelectedItem().getNomCategorie()+ "?");
            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {

                ServiceCategorie us = new ServiceCategorie();
                Categorie p = new Categorie(Categorie.getSelectionModel().getSelectedItem().getId(), nomCat1.getText(), referenceCat1.getText());
                us.modifierCat(p);
                System.out.println(p);
                System.out.println("___________a_________");

                Afficher();

            }

            nomCat1.setText("");
            referenceCat1.setText("");
           

            //afficher.setVisible(false);
            tab.getSelectionModel().select(0);

        }
    } 
    
    
        @FXML
    private void afficher_LesCategories(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Categorie.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
     @FXML
    private void afficher_LesSousCategories(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/SousCategorie.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
     @FXML
    private void afficher_Produits(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Produit.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
    
    
    
     @FXML
    private void retourneCategories(ActionEvent event) {
        tab.getSelectionModel().select(0);
        // trying.setVisible(true);

    }

      @FXML
    private void quit(MouseEvent event) throws IOException {
        
        Parent root = FXMLLoader.load(getClass().getResource("/pidev/login.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void home(MouseEvent event) throws IOException {
         Parent root = FXMLLoader.load(getClass().getResource("/pidev/acc.fxml"));
        main.stage.setScene(new Scene(root));
    }
}
