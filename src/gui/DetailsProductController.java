/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity_ib.Produit;
import Entity_ib.Session;
import Service.ServiceProduit;
import Service.ServiceRating;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.util.Duration;
import ibtih.MyConnection;
import ibtih.Nawara;
import org.controlsfx.control.Rating;

/**
 * FXML Controller class
 *
 * @author user
 */
public class DetailsProductController implements Initializable {

    Produit r = new Produit();
    ServiceProduit Rs = new ServiceProduit();
    @FXML
    private ImageView monimage;
    @FXML
    private Label idTitre;
    @FXML
    private Label idPrix;
    @FXML
    private Label idStock;

    @FXML
    private ImageView idImg;

    @FXML
    private Rating rating;
    @FXML
    private Label ratingLabel;
    @FXML
    private ScrollPane pane;

    private void aaaa(MouseEvent event) {
        System.exit(0);
    }

    /* private Commentaire c;
    CommentaireService co = new CommentaireService();
    UserService userService = new UserService();
    User user;*/
    private Produit Produit;
    private static DetailsProductController instance;
    private ObservableList<Produit> data;
    Connection cn = MyConnection.getInstance().getConnection();
    ObservableList<Produit> items = FXCollections.observableArrayList();

    public DetailsProductController() {
        instance = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rating.ratingProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> url, Number oldValue, Number newValue) {

                ratingLabel.setText(newValue.toString());
                
            }
        });

        //javafx.scene.image.Image im = new javafx.scene.image.Image("http://localhost/dotcom_2019/web/img/"+Produit.getPhotoProduit());
        // monimage.setImage(im);
        try {
            ServiceRating sr = new ServiceRating();
            
            
            TilePane b = new TilePane();
            b.setPadding(new javafx.geometry.Insets(30));
            TilePane c = new TilePane();
            FadeTransition ft = new FadeTransition(Duration.millis(1500));
            
            data = loadProduit(Nawara.p);
            ArrayList<Entity_ib.Rating> rates = sr.GetAllByProduct(1, Nawara.p.getId());
            if(!rates.isEmpty()){
                rating.setVisible(false);
            }
            for (Produit d : data) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("box.fxml"));
                    ;
                    Parent root = (Pane) loader.load();
                    BoxController DHC = loader.getController();
                    DHC.LoadValues(d);
                    c.getChildren().removeAll();
                    c.getChildren().add(root);
                } catch (IOException ex) {
                    Logger.getLogger(DetailsProductController.class.getName()).log(Level.SEVERE, null, ex);
                    
                }
            }
            c.setPrefColumns(3);
            c.setPadding(new javafx.geometry.Insets(0));
            c.setHgap(10);
            c.setVgap(80);
            b.getChildren().add(c);
            b.setPrefWidth(800);
            pane.setContent(b);
        } catch (SQLException ex) {
            Logger.getLogger(DetailsProductController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Produit getProduit() {
        return this.Produit;
    }

    public ObservableList<Produit> loadProduit(Produit pi) throws SQLException {
        
       // System.out.println("helloooooooo"+getScat());
        PreparedStatement pt = cn.prepareStatement("SELECT produit.nomProduit, produit.photoProduit  from produit where produit.Souscategorie_id=? limit 3");
        pt.setInt(1, Nawara.p.getSousCatg());
        ResultSet resultat = pt.executeQuery();
        while (resultat.next()) {
            String nom = resultat.getString("nomProduit");
            String img = resultat.getString("photoProduit");
            Produit p = new Produit(nom, img);
            items.add(p);
        }
        
        System.out.println("size" + items.size());
        return items;
        
    }

    public void setProduit(Produit produit) {

        this.Produit = produit;
        System.out.println("i'm first SetProduit");
        idTitre.setText(this.Produit.getNomProduit());
        idPrix.setText(String.valueOf(this.Produit.getPrixVente()));
        idStock.setText(String.valueOf(this.Produit.getQuantite()));
        
        

        Image im = new Image("http://localhost/pidevweb/web/img/" + Produit.getPhotoProduit());
        idImg.setImage(im);
        Nawara.p = produit;
        //idDateS.setValue(this.programme.getDateSortie());
        //idDateP.setValue(this.programme.getDateProjection());
        System.out.println(Produit.getId() + " sub " + Produit.getSousCatg());
        System.out.println(Nawara.p.getSousCatg() + " works");

    }


    static public DetailsProductController getInstance() {
        return instance;
    }

    @FXML
    private void Rating(MouseEvent event) throws SQLException {
        
        ServiceRating psp = new ServiceRating();
        System.out.println(this.Produit.getId());
        System.out.println(ratingLabel.getText());
        try {
            psp.Rate(this.Produit.getId(), 1, Float.valueOf(ratingLabel.getText().toString()));
            rating.setVisible(false);
        } catch (Exception ex) {
            
            Logger.getLogger(DetailsProductController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}


/*  @FXML
    public void handleBtnAction (ActionEvent event) throws SQLException{
        UserSession session = UserSession.getInstance(null,null,null,null,null,null,null);
        c.setId_programme(programme.getId());
        int idd = userService.get(session.getUserName()).getId();
        c.setUser(idd);
        c.setContenu(contenu.getText());
        co.ajouter(c);
    }
 */
