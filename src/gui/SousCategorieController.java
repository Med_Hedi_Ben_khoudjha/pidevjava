/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity_ib.Categorie;
import Entity_ib.SousCategorie;
import Service.ServiceCategorie;
import Service.SousCategorieService;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pidev.main;

/**
 * FXML Controller class
 *
 * @author dorra
 */
public class SousCategorieController implements Initializable {

    @FXML
    private Button btnOverview;
    @FXML
    private Button btnCustomers;
    @FXML
    private Button btnMenus;
    @FXML
    private Button btnPackages;
    @FXML
    private Button btnOrders;
    @FXML
    private Button btnSettings;
    @FXML
    private Button btnSignout;
    @FXML
    private Pane pnlCustomer;
    @FXML
    private Pane pnlOrders;
    @FXML
    private Pane pnlMenus;
    @FXML
    private Pane pnlOverview;
  
    @FXML
    private TableColumn<SousCategorie, String> nom_ssc;
    @FXML
    private TableColumn<SousCategorie, String> reference_ssc;
    
     ObservableList<SousCategorie> ListSouscategories = FXCollections.observableArrayList();
     ObservableList<String> Listcategories = FXCollections.observableArrayList();
    SousCategorieService scatgs = new SousCategorieService();
    SousCategorie scat = new SousCategorie();
   ServiceCategorie catgs = new ServiceCategorie();
    Categorie cat = new Categorie();
    @FXML
    private TableView<SousCategorie> SousCategorie;
    @FXML
    private TabPane tab;
    @FXML
    private Tab formajout;
    @FXML
    private Pane pnlCustomer1;
    @FXML
    private Pane pnlOrders1;
    @FXML
    private Pane pnlMenus1;
    @FXML
    private Pane pnlOverview1;
    @FXML
    private Tab formajout1;
    @FXML
    private Pane pnlCustomer11;
    @FXML
    private Pane pnlOrders11;
    @FXML
    private Pane pnlMenus11;
    @FXML
    private Pane pnlOverview11;
    
    
    
    
    
    
    
    @FXML
    private Button ajouterSousCategorie;
    @FXML
    private TextField nomSSC;
    @FXML
    private TextField referenceSSC;
    @FXML
    private ComboBox<String> Cat;
    @FXML
    private Button btn_modifierSSC;
    @FXML
    private TextField nomSSC1;
    @FXML
    private TextField referenceSSC1;
    @FXML
    private Button ajouterSSC;
    @FXML
    private Button btn_suppSSC;
   
 Stage dialogStage = new Stage();
    Scene scene;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      Cat.setItems(Listcategories);
        List<Categorie> myList = catgs.getCategorie();
        List<String> myStrings = new ArrayList<>();
        for (Categorie s : myList) {
            myStrings.add(s.getNomCategorie());
        }

        Cat.setItems(FXCollections.observableArrayList(myStrings));

        Afficher();
    }    

     void Afficher() {

        ListSouscategories = FXCollections.observableArrayList(scatgs.getAll());
         
       
        
        
        nom_ssc.setCellValueFactory(new PropertyValueFactory<>("nomSousCategorie"));
        nom_ssc.cellFactoryProperty();

        reference_ssc.setCellValueFactory(new PropertyValueFactory<>("referenceSousCategorie"));
        reference_ssc.cellFactoryProperty();

       

        // col_dateProduit.setCellValueFactory(new PropertyValueFactory<>("dateProduit"));
        // col_dateProduit.cellFactoryProperty();
        SousCategorie.setItems(ListSouscategories);

    }

    @FXML
    private void afficher_formulaire_ajouter(ActionEvent event) {
        tab.getSelectionModel().select(1);
        
        // trying.setVisible(true);

    }
    
     @FXML
    private void Enregistrer_AjoutSSC(ActionEvent event) {
        
        Categorie s = catgs.getCategorieByName(Cat.getSelectionModel().getSelectedItem());
        SousCategorie p = new SousCategorie(nomSSC.getText(), referenceSSC.getText(), s.getId());
if (nomSSC.getText().equals("") || referenceSSC.getText().equals("") ) { 
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Alerte");
            alert.setHeaderText("Vous devez remplir TOUT LES CHAMPS SVP");
             Optional<ButtonType> result = alert.showAndWait();
             if (result.get() == ButtonType.OK) {
             tab.getSelectionModel().select(1);
              Afficher();
        nomSSC.setText("");
        referenceSSC.setText("");}}
  
  else {
        scatgs.ajoutersouscategorie(p);
        Afficher();
        nomSSC.setText("");
        referenceSSC.setText("");
        
        tab.getSelectionModel().select(0);

    }}

    @FXML
    private void supprimer(ActionEvent event) {
        if (!SousCategorie.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Suppression d'une Sous Categorie");
            alert.setHeaderText("Etes-vous sur de vouloir supprimer la sous categorie "
                    + SousCategorie.getSelectionModel().getSelectedItem().getNomSousCategorie()+ "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                SousCategorieService su = new SousCategorieService();
                System.out.println(SousCategorie.getSelectionModel().getSelectedItem().getId());
                su.supprimerSS(SousCategorie.getSelectionModel().getSelectedItem().getId());

                Afficher();

            }
            btn_suppSSC.setVisible(true);
            // modifier.setVisible(false);
            //annuler.setVisible(false);
            //afficher.setVisible(false);
            //ajouter.setVisible(true);*/
        }
    }
    
    
    
     @FXML
    private void afficher_formulaire_modifier(ActionEvent event) {

        nomSSC1.setText(SousCategorie.getSelectionModel().getSelectedItem().getNomSousCategorie());
        referenceSSC1.setText(SousCategorie.getSelectionModel().getSelectedItem().getReferenceSousCategorie());
       
        tab.getSelectionModel().select(2);
        //prenom.setText(utilisateur.getSelectionModel().getSelectedItem().getPrenom());

        // valider.setVisible(false);
       // btn_suppProd.setVisible(false);
        //  modifier.setVisible(false);
        //annuler.setVisible(false);
        // ajouter.setVisible(true);
    }

    @FXML
    private void Enregistrer_modification(ActionEvent event) {
        if (!SousCategorie.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("suppression d'une sous categorie");
            alert.setHeaderText("Etes-vous sur de vouloir modifier "
                    + SousCategorie.getSelectionModel().getSelectedItem().getNomSousCategorie()+ "?");
            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {

                SousCategorieService us = new SousCategorieService();
                SousCategorie p = new SousCategorie(SousCategorie.getSelectionModel().getSelectedItem().getId(), nomSSC1.getText(), referenceSSC1.getText());
                us.modifierSSC(p);
                System.out.println(p);
                System.out.println("___________a_________");

                Afficher();

            }

            nomSSC1.setText("");
            referenceSSC1.setText("");
           

            //afficher.setVisible(false);
            tab.getSelectionModel().select(0);

        }
    } 
       @FXML
    private void afficher_LesCategories(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Categorie.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
    @FXML
    private void afficher_LesSousCategories(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/SousCategorie.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
    @FXML
    private void afficher_Produits(ActionEvent event) throws IOException {
       Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Produit.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();

    }
    
      @FXML
    private void retourneSousCategories(ActionEvent event) {
        tab.getSelectionModel().select(0);
        // trying.setVisible(true);

    }

     @FXML
    private void quit(MouseEvent event) throws IOException {
        
        Parent root = FXMLLoader.load(getClass().getResource("/pidev/login.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void home(MouseEvent event) throws IOException {
         Parent root = FXMLLoader.load(getClass().getResource("/pidev/acc.fxml"));
        main.stage.setScene(new Scene(root));
    }
    
}
