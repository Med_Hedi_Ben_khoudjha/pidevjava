/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity_ib.Produit;
import Service.ServiceProduit;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.util.Duration;
import ibtih.MyConnection;

/**
 * FXML Controller class
 *
 * @author user
 */
public class CatalogueController implements Initializable {
    ServiceProduit ProdService = new ServiceProduit();
    Produit u = new Produit();
    ListView<Produit> list=new ListView<>();
    ObservableList<Produit> items = FXCollections.observableArrayList();
    private ObservableList<Produit> data;
        Connection cn = MyConnection.getInstance().getConnection();
    @FXML
    private ImageView monimage;
    @FXML
    private ScrollPane pane;
    @FXML
    private Label btn_exit;
    @FXML
    private void handleButtonAction(MouseEvent event) {
      System.exit(0);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        javafx.scene.image.Image im = new javafx.scene.image.Image("http://localhost/pidevweb/web/img/"+u.getPhotoProduit());
        monimage.setImage(im);
        try {
            TilePane b = new TilePane();
            b.setPadding(new javafx.geometry.Insets(30));
            TilePane c = new TilePane();
            FadeTransition ft = new FadeTransition(Duration.millis(1500));
           data = loadProduit();
            for ( Produit d : data) {                
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("box.fxml"));
                    Parent root = (Pane) loader.load();
                    BoxController DHC = loader.getController();
                    DHC.LoadValues(d);
                    c.getChildren().removeAll();
                    c.getChildren().add(root);
                } catch (IOException ex) {
                    Logger.getLogger(CatalogueController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            c.setPrefColumns(3);
            c.setPadding(new javafx.geometry.Insets(0));
            c.setHgap(20);
            c.setVgap(120);
            b.getChildren().add(c);
            b.setPrefWidth(1000);
            pane.setContent(b);    
                }catch (SQLException ex) {
                    Logger.getLogger(CatalogueController.class.getName()).log(Level.SEVERE, null, ex);
                }
    }   
           public ObservableList<Produit> loadProduit() throws SQLException {
          PreparedStatement pt = cn.prepareStatement("SELECT produit.nomProduit, produit.photoProduit  from produit");       
        ResultSet resultat = pt.executeQuery();
        while (resultat.next()) {
            String nom = resultat.getString("nomProduit");
            String img=resultat.getString("photoProduit");
            Produit p=new Produit(nom,img);
            items.add(p);
        }
         return items;
    }    
/*
    @FXML
    private void pdf(MouseEvent event) {
    } 
    
    @FXML
    private void monimage(MouseEvent event) throws IOException {
      Pagination p = new Pagination("/Presentation/monProfil.fxml");
        ((Node)(event.getSource())).getScene().getWindow().hide();        
    }*/
}