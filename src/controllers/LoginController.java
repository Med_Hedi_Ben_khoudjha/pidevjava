/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Entity_ib.Session;
import Entity_ib.Utilisateur;
import Service.ServiceUser;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Zaineb
 */
public class LoginController implements Initializable {

    @FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Button btnSignin;
    @FXML
    private Label btnForgot;
    @FXML
    private Button btnFB;
    @FXML
    private Label lblErrors;
     Stage dialogStage = new Stage();
    Scene scene;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
          
        txtUsername.setText("Username");
        txtPassword.setText("Pass");
    }   
    private void connexionUtilisateur(ActionEvent event) throws Exception {
        
         ServiceUser us = new ServiceUser();
        Utilisateur u = new Utilisateur();
        u.setUsername(txtUsername.getText());
        u.setPassword(txtPassword.getText());
        
        if (us.login(u) == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("user inccorrect");
            alert.setContentText("please check your informations!");
            alert.showAndWait();
        } else {
            Session.start(u.getId());
            
            if (us.login(u) != null && us.testMotDePasse(txtPassword.getText(),u.getPassword())) {
              
                if (us.login(u).getRoles().contains("ROLE_ADMIN")) {
                    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Utilisateur.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();
                } else {
                    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Utilisateur.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();
                }

            }
        }}
        

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }
    
}
