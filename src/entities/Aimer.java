/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Malek
 */
public class Aimer {
    private int id;
    private Membre membre ;
    private Publication publication ;
    private String date ;

    public Aimer() {
    }
    
    public Aimer(int id,Membre membre, Publication publication, String date) {
        this.id=id;
        this.membre = membre;
        this.publication = publication;
        this.date = date;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Aimer{" + "id=" + id + ", membre=" + membre + ", publication=" + publication + ", date=" + date + '}';
    }

   

  
    
    
}
