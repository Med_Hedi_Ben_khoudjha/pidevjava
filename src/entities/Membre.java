/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import static jdk.nashorn.internal.runtime.Debug.id;

/**
 *
 * @author Malek
 */
public class Membre {
    private int id;
    private String nom;
    private String prenom;
    private String adresse;
    private String mail;
    private String role;
    private String psw;
    private String pseudo;
    private boolean permis;
    private boolean motorise;
    
     public Membre() {
    }

    public Membre(int id, String nom, String prenom, String adresse, String mail, String role, String psw, String pseudo, boolean permis, boolean motorise) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.mail = mail;
        this.role = role;
        this.psw = psw;
        this.pseudo = pseudo;
        this.permis = permis;
        this.motorise = motorise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public boolean isPermis() {
        return permis;
    }

    public void setPermis(boolean permis) {
        this.permis = permis;
    }

    public boolean isMotorise() {
        return motorise;
    }

    public void setMotorise(boolean motorise) {
        this.motorise = motorise;
    }

   

    @Override
    public String toString() {
        return ""+this.getNom();
    }
    
    
    
    
    
    
}
