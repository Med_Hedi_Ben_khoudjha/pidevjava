/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

/**
 *
 * @author Malek
 */
public class Publication extends RecursiveTreeObject<Publication>{
    private int id;
    private String date ;
    private String localisation;
    private String image;
    private String contenu;
    private Membre membre;
    private int id_m ;

    public Publication() {
        membre = new Membre();
    }

    public Publication(int id, String date, String localisation, String image, String contenu, int membre) {
        this.id = id;
        this.date = date;
        this.localisation = localisation;
        this.image = image;
        this.contenu = contenu;
        this.id_m = membre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public int getMembre() {
        return id_m;
    }

    public void setMembre(int membre) {
        this.id_m = membre;
    }

    @Override
    public String toString() {
        return "publication{" + "id=" + id + ", date=" + date + ", localisation=" + localisation + ", image=" + image + ", contenu=" + contenu + ", membre=" + membre + '}';
    }
    
    
}
