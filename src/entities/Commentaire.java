/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author Malek
 */
public class Commentaire {
    private int id;
    private Membre membre ;
    private Publication publication ;
    private Date date ;
    private String message ;

    public Commentaire() {
        membre=new Membre();
    }

    public Commentaire(int id,Membre membre, Publication publication, Date date, String message) {
        this.id=id;
        this.membre = membre;
        this.publication = publication;
        this.date = date;
        this.message = message;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Commentaire{" + "id=" + id + ", membre=" + membre + ", publication=" + publication + ", date=" + date + ", message=" + message + '}';
    }

   
    
    
    
    
}
