/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Crud;

import entities.Commentaire;
import entities.Membre;
import entities.Publication;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utils.MyConnection;

/**
 *
 * @author Malek
 */
public class CommentaireCrud {
     MyConnection myCNX= MyConnection.getMyConnection();
     public void ajouterCommentaire(Commentaire c)
    {
        try {
            String requet = "insert into commtaire (id_publication,id_membre,message,date)VALUES('"+c.getPublication().getId()+"','"+c.getMembre().getId()+"','"+c.getMessage()+"','"+c.getDate()+"')";


            Statement pr= myCNX.getCnx().createStatement();
            System.out.println(pr.toString());
            pr.executeUpdate(requet);
            System.out.println("commentaire ajouté");
                    } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
   
    public void supprimerCommentaire(int idd)
    {
        try {
            String requet2 = "DELETE from commtaire where id=?";
            MyConnection myCNX= MyConnection.getMyConnection();
            PreparedStatement pst= myCNX.getCnx().prepareStatement(requet2);
            pst.setInt(1,idd);  //l 1 hia l champ loulenii fel requete mteena 
            pst.executeUpdate();
            System.out.println("Publication Supprimee");
                    } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
  
   
    public void modifierCommentaire(Commentaire c, int idd)
    {
        try {
            String requet3 = "update commtaire set message=?  where id=?"; //l ? tebaa l preparedStatement 
            MyConnection myCNX= MyConnection.getMyConnection();
            PreparedStatement pst= myCNX.getCnx().prepareStatement(requet3);
            
            pst.setInt(2, c.getId());
            pst.setString(1, c.getMessage());
            pst.executeUpdate();
            System.out.println("commentaire modifie");
                    } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
      public List<Commentaire> afficherCommentaire()
     {
         String requet4="Select * from commtaire";
         MyConnection myCNX= MyConnection.getMyConnection();
         ArrayList<Commentaire> myList= new ArrayList();
        
             Statement st1;
        try {
            st1 = myCNX.getCnx().createStatement();
            ResultSet rs= st1.executeQuery(requet4);
            while(rs.next())
            {
                Commentaire c =new Commentaire();
                c.setId(rs.getInt("id"));
                c.setMessage(rs.getString("message"));
                c.setMembre(getMember(rs.getInt("id_membre")));
                c.setPublication(getPublication(rs.getInt("id_Publication")));
                c.setDate(rs.getDate("Date"));
                myList.add(c);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
            return myList;         
     }
      
      /* public List<Commentaire> afficherCommentaire2(int IdMembre)
     {
         String requet4="Select * from commtaire Where id=="+IdMembre+";";
         MyConnection myCNX= MyConnection.getMyConnection();
         ArrayList<Commentaire> myList= new ArrayList();
        
             Statement st1;
        try {
            st1 = myCNX.getCnx().createStatement();
            ResultSet rs= st1.executeQuery(requet4);
            while(rs.next())
            {
                Commentaire c =new Commentaire();
                c.setId(rs.getInt("id"));
                c.setMessage(rs.getString("message"));
                c.setDate(rs.getString("Date"));
                myList.add(c);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
            return myList;         
     }*/
       
        public Membre getMember(int idMembre) {
        String requet4 = "Select * from membre where id=" + idMembre + ";";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Commentaire> myList = new ArrayList();
        Membre p = new Membre();
        Statement st1;
        try {
            st1 = myCNX.getCnx().createStatement();
            ResultSet rs = st1.executeQuery(requet4);
            while (rs.next()) {
                p.setId(rs.getInt("id"));
                p.setNom(rs.getString("nom"));
                p.setPrenom(rs.getString("prenom"));
                p.setMail(rs.getString("mail"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
        
         public Publication getPublication(int idPublication) {
        String requet4 = "Select * from membre where id=" + idPublication+ ";";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Commentaire> myList = new ArrayList();
        Publication p = new Publication();
        Statement st1;
        try {
            st1 = myCNX.getCnx().createStatement();
            ResultSet rs = st1.executeQuery(requet4);
            while (rs.next()) {
                p.setId(rs.getInt("id"));
                p.setDate(rs.getString("Date"));
                p.setContenu(rs.getString("contenu"));
             
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
}
