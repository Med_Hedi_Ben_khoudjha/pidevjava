/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Crud;

import entities.Membre;
import entities.Publication;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.MyConnection;

/**
 *
 * @author Malek
 */
public class PublicationCrud {

    MyConnection myCNX = MyConnection.getMyConnection();
    private Object m;

    public void ajouterPublication(Publication p) {
        try {
            String requet = "insert into Publication (id,date,localisation,image,contenu,id_membre)VALUES('" + p.getId() + "','" + p.getDate() + "','" + p.getLocalisation() + "','" + p.getImage() + "','" + p.getContenu() + "','" + p.getMembre() + "')";

            Statement pr = myCNX.getCnx().createStatement();
            System.out.println(pr.toString());
            pr.executeUpdate(requet);
            System.out.println("pub ajoutee");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void supprimerPublication(int idd) {
        try {
            String requet2 = "DELETE from publication where id=?";
            // MyConnection myCNX = MyConnection.getMyConnection();
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requet2);
            pst.setInt(1, idd);  //l 1 hia l champ loulenii fel requete mteena 

            pst.executeUpdate();
            System.out.println("Publication Supprimee");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void modifierPublication(Publication p, int idd) {
        try {
            String requet3 = "update publication set localisation=?,image=?,contenu=?  where id=?"; //l ? tebaa l preparedStatement 
            // MyConnection myCNX = MyConnection.getMyConnection();
            PreparedStatement pst = myCNX.getCnx().prepareStatement(requet3);
           // pst.setString(1, p.getDate());
            pst.setString(1, p.getLocalisation());
            pst.setString(2, p.getImage());
            pst.setString(3, p.getContenu());

            pst.setInt(4, p.getId());

            pst.executeUpdate();
            System.out.println("Publication modifiee");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<Publication> afficherPublication() {
        String requet4 = "Select * from publication ";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Publication> myList = new ArrayList();
        Statement st1;
        try {
            st1 = myCNX.getCnx().createStatement();
            ResultSet rs = st1.executeQuery(requet4);
            while (rs.next()) {
                Publication p = new Publication();
                p.setId(rs.getInt("id"));
                p.setDate(rs.getString("date"));
                p.setMembre(rs.getInt("id_membre"));
                p.setImage(rs.getString("image"));
                p.setLocalisation(rs.getString("localisation"));
                p.setContenu(rs.getString("contenu"));
                myList.add(p);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }

    public List<Publication> afficherPublication2(int idMembre) {
        String requet4 = "Select * from publication where id=" + idMembre + ";";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Publication> myList = new ArrayList();

        Statement st1;
        try {
            st1 = myCNX.getCnx().createStatement();
            ResultSet rs = st1.executeQuery(requet4);
            while (rs.next()) {
                Publication p = new Publication();
                p.setId(rs.getInt("id"));
                p.setImage(rs.getString("image"));
                p.setLocalisation(rs.getString("localisation"));
                p.setContenu(rs.getString("contenu"));
                myList.add(p);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }

    public Membre getMember(int idMembre) {
        String requet4 = "Select * from membre where id=" + idMembre + ";";
        MyConnection myCNX = MyConnection.getMyConnection();
        ArrayList<Publication> myList = new ArrayList();
        Membre p = new Membre();
        Statement st1;
        try {
            st1 = myCNX.getCnx().createStatement();
            ResultSet rs = st1.executeQuery(requet4);
            while (rs.next()) {
                p.setId(rs.getInt("id"));
                p.setNom(rs.getString("nom"));
                p.setPrenom(rs.getString("prenom"));
                p.setMail(rs.getString("mail"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
}
