/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import B.D.DbConnect;
import Entity.agence;
import Entity.membre;
import Entity.offre;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import static crud.gestion_de_compte.email;
import static crud.gestion_de_compte.id_membre;
import static crud.gestion_de_compte.nom;
import static crud.gestion_de_compte.prenom;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 *
 * @author MED
 */
public class gestion_agence {

    
    public static String latitude;
    public static String longitude;
    public static String cityName;
    public static String state;
    
    gestion_de_compte h = new gestion_de_compte();
            DbConnect j = new DbConnect();
    public gestion_agence() {
    }

    public void ajouter_agence(String nom,
            String type,
            String adress,
            String info,
            String longitude,
            String latitude,
            String telephone,
            int id) {
        DbConnect l = new DbConnect();
        Connection k = l.getCnx();
        try {
            String sql = "INSERT INTO agence "
                    + "(adresse,"
                    + "telephone,"
                    + "type,"
                    + "info,"
                    + "nom,"
                    + "longitude,"
                    + "latitude,"
                    + "id_membre) VALUES(?,?,?,?,?,?,?,?)";
            PreparedStatement statement = k.prepareStatement(sql);
            //en spécifiant bien les types SQL cibles
            statement.setString(1, adress);
            statement.setString(2, telephone);
            statement.setString(3, type);
            statement.setString(4, info);
            statement.setString(5, nom);
            statement.setString(6, latitude);
            statement.setString(7, longitude);
            statement.setInt(8,id);

            statement.executeUpdate();
            statement.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

    }

    //suppression d'agence       
    public void supprimer_agence(int d) {
        DbConnect l = new DbConnect();
        Connection k = l.getCnx();
        try {
            String sql = "DELETE agence WHERE id = ?";

            PreparedStatement statement = k.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
            statement.setInt(1, d);

            statement.executeUpdate();
            statement.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
//insertion d'offre

    public void insert_offre(String type,String info,String date,int agence_id) {
        DbConnect l = new DbConnect();
        Connection k = l.getCnx();
        try {
           
            
            String sql = "INSERT INTO offre (type,date,id_agence,info )VALUES(?,?,?,?)";

            PreparedStatement statement = k.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
            statement.setString(1, type);
            statement.setString(2, date);
            statement.setInt(3, agence_id);
            statement.setString(4, info);

            statement.executeUpdate();

            statement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
 

//insertion d'offre

    public void insert_reservation(String origine,
            String destination,
            String date_d,
            String date_r,
            String montant,
            String date_res,
            int id_m) {
        DbConnect l = new DbConnect();
        Connection k = l.getCnx();
        try {
           
            
            String sql = "INSERT INTO reservation_vols"
                    + " (origine,"
                    + "destination,"
                    + "date_d,"
                    + "date_r,"
                    + "montant,"
                    + "date_res,"
                    + "id_m )VALUES(?,?,?,?,?,?,?)";

            PreparedStatement statement = k.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
            statement.setString(1, origine);
            statement.setString(2, destination);
            statement.setString(3, date_d);
            statement.setString(4, date_r);
            statement.setString(5, montant);
            statement.setString(6, date_res);
            statement.setInt(7, id_m);

            statement.executeUpdate();

            statement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
 public void insert_payement(int id_membre,
            String date,
            String montant,
            int id_r_v,
            String num_carte)
 {
        DbConnect l = new DbConnect();
        Connection k = l.getCnx();
        try {
           
            
            String sql = "INSERT INTO payement"
                    + " (id_membre,"
                    + "date,"
                    + "montant,"
                    + "id_r_v ,"
                    + "num_carte)VALUES(?,?,?,?,?)";

            PreparedStatement statement = k.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
            statement.setInt(1, id_membre);
            statement.setString(2, date);
            statement.setString(3, montant);
            statement.setInt(4, id_r_v);
            statement.setString(5, num_carte);
            

            statement.executeUpdate();

            statement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void reclamation(String text, String sujet) {
        DbConnect l = new DbConnect();
        Connection k = l.getCnx();
        Date d = new Date();
        System.out.println(d.toString());
        try {

            String sql = "INSERT INTO reclamation (text, date,id_membre,sujet )VALUES(?,?,?,?)";

            PreparedStatement statement = k.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
            statement.setString(1, text);
            statement.setString(2, d.toString());
            statement.setInt(3, gestion_de_compte.id_membre);
            statement.setString(4, sujet);

            statement.executeUpdate();
            statement.close();
            String host = "smtp.gmail.com";
            String user = gestion_de_compte.email;
            String pass = "E183JMT2501";
            String to = "mohamedheidibenkodjha@gmail.com";
            String from = "mohamedhedi.benkhoudja@esprit.tn";
            String subject = sujet;
            String messageText = text;
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            //  java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(messageText);

            javax.mail.Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            System.out.println("message send successfully");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (MessagingException ex) {
            Logger.getLogger(gestion_agence.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
    public void givenIP_whenFetchingCity_thenReturnsCityData(String ipA)
            throws IOException, GeoIp2Exception {

        String ip = ipA;
        String dbLocation = "C:\\Users\\MED\\Downloads\\GeoLite2-City_20190219\\GeoLite2-City_20190219\\GeoLite2-City.mmdb";

        File database = new File(dbLocation);
        DatabaseReader dbReader = new DatabaseReader.Builder(database)
                .build();

        InetAddress ipAddress = InetAddress.getByName(ip);
        CityResponse response = dbReader.city(ipAddress);

        String countryName = response.getCountry().getName();
         cityName = response.getCity().getName();
        String postal = response.getPostal().getCode();
         state = response.getLeastSpecificSubdivision().getName();

        latitude
                = response.getLocation().getLatitude().toString();
        longitude
                = response.getLocation().getLongitude().toString();
        System.out.println(countryName);
        System.out.println(cityName);
        System.out.println(latitude);
        System.out.println(longitude);
        System.out.println(state);
        System.out.println(Double.parseDouble(gestion_agence.latitude));

    }
    
    public List<agence>  select(){
         DbConnect l = new DbConnect();
        Connection k = l.getCnx();
         agence a;
List <agence> list_agence=new ArrayList<>();

        ResultSet resultSet = null;
        try {
            PreparedStatement pr = l.cnx.prepareStatement("select "
                    + "adresse,"
                    + "telephone,"
                    + "type,"
                    + "nom,"
                    + "longitude,"
                    + "latitude,"
                    + "id"
                    + " from agence");

           
            resultSet = pr.executeQuery();

          while (resultSet.next()) {
                 a = new agence(resultSet.getInt("id"),
                        resultSet.getString("type"),
                        resultSet.getString("adresse"),
                        null,
                        resultSet.getString("telephone"),
                        resultSet.getString("nom"),
                        resultSet.getString("longitude"),
                        resultSet.getString("latitude"));                
               
                        list_agence.add(a);
                
                
            }
        }

         catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
return list_agence;
    }
    
    public List<Integer>  select_id(int id){
         agence a;
List <Integer> list_agence=new ArrayList<Integer>();

        ResultSet resultSet = null;
        try {
            PreparedStatement pr = j.getCnx().prepareStatement("select "
                    + "id from reservation_vols where id_m=?");

                       pr.setInt(1, id);

            resultSet = pr.executeQuery();

          while (resultSet.next()) {
                          
               
                        list_agence.add(resultSet.getInt("id"));
                
                
            }
        }

         catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
return list_agence;
    }
    
    public List<String>  select_indic(){
         agence a;
List <String> list=new ArrayList<String>();

        ResultSet resultSet = null;
        try {
            PreparedStatement pr = j.getCnx().prepareStatement("select "
                    + "Telephone_Code,"
                    + "Commo_Name"
                    + " from tc_pays");

           
            resultSet = pr.executeQuery();

          while (resultSet.next()) {
                        System.out.println(resultSet.getString("Commo_Name")+" "+resultSet.getString("Telephone_Code"));
                        
                list.add(resultSet.getString("Commo_Name")+" "+resultSet.getString("Telephone_Code"));
                
            }
        }

         catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
return list;
    }
    public List<String>  select_pays(){
         agence a;
List <String> list=new ArrayList<String>();

        ResultSet resultSet = null;
        try {
            PreparedStatement pr = j.getCnx().prepareStatement("select "
                    + "Telephone_Code,"
                    + "Commo_Name"
                    + " from tc_pays");

           
            resultSet = pr.executeQuery();

          while (resultSet.next()) {
                        
                list.add(resultSet.getString("Commo_Name"));
                
            }
        }

         catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
return list;
    }
    public static offre  select_offre(int id){
         offre a = null;
                 DbConnect l = new DbConnect();

        Connection j = l.getCnx();

        ResultSet resultSet = null;
        try {
            PreparedStatement pr = j.prepareStatement("select * from offre where id_agence=?");

                       pr.setInt(1, id);

            resultSet = pr.executeQuery();

          while (resultSet.next()) {
                        System.out.println(resultSet.getString("type")+
                                " "+resultSet.getString("info")+
                                ""+resultSet.getString("date"));
                        
a=new offre(
        resultSet.getString("type"),
        resultSet.getString("info"),
        resultSet.getString("date")
       );
            }
        }

         catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
return a;
    }


    
}
