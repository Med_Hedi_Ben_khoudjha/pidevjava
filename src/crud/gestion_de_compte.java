/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import B.D.DbConnect;

import Entity.membre;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import pidev.main;

/**
 *
 * @author MED
 */
public class gestion_de_compte {

    DbConnect l = new DbConnect();
    Connection k = l.getCnx();
    public static String nom ;
    public static String prenom ;
    public static int id_membre ;
    public static String email ;

    public void inscrire(membre u) {

        try {
            String sql = "INSERT INTO membre (id,"
                    + "nom,"
                    + "prenom,"
                    + "adresse,"
                    + "email,"
                    + "role,"
                    + "password,"
                    + "username,"
                    + "permis,"
                    + "motorise) VALUES(?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement statement = k.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
            statement.setInt(1, 0);
            statement.setString(2, u.getNom());
            statement.setString(3, u.getPrenom());
            statement.setString(4, u.getAdresse());
            statement.setString(5, u.getEmail());
            statement.setString(6, u.getRole());
            statement.setString(7, u.getPsw());
            statement.setString(8, u.getPseudo());
            statement.setBoolean(9, u.isPermis());
            statement.setBoolean(10, u.isMotorise());

            statement.executeUpdate();
            statement.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    //modification de mot passe en cas d'oublie

    public void Modifier_Mot_passe(membre d, int id, String psw) {

        try {
            String sql = "UPDATE membre SET password=? WHERE id=?";

            PreparedStatement statement = k.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
            statement.setInt(2, id);
            statement.setString(1, psw);

            statement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

        }
    }

    public boolean s_authentifier(String p, String pa) {
        String pseudo = "";
        String psw = "";

        ResultSet resultSet = null;
        try {
            DbConnect j = new DbConnect();

            PreparedStatement pr = j.getCnx().prepareStatement("select username,password,nom,prenom,id,email from membre where username=? and password=? ");

            pr.setString(1, p);
            pr.setString(2, pa);
            resultSet = pr.executeQuery();

            if (resultSet.next()) {
                System.out.println(resultSet.getString("username"));
                System.out.println(resultSet.getString("password"));
                System.out.println(resultSet.getString("nom"));
                System.out.println(resultSet.getString("prenom"));

                pseudo = resultSet.getString("username");
                psw = resultSet.getString("password");
                nom = resultSet.getString("nom");
                prenom = resultSet.getString("prenom");
                email = resultSet.getString("email");
                id_membre = resultSet.getInt("id");
                
                return true;
            } else {

                System.out.println("nn");
                return false;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;

        }

    }

    public boolean verifier_pseudo(String pseudo) {
        ResultSet resultSet = null;
        try {
            DbConnect j = new DbConnect();

            PreparedStatement pr = j.getCnx().prepareStatement("select username "
                    + "from membre where username=?");

            pr.setString(1, pseudo);

            resultSet = pr.executeQuery();

            if (resultSet.next()) {
                System.out.println(resultSet.getString("username"));
                pseudo = resultSet.getString("username");

                return true;
            } else {

                System.out.println("nn");
                return false;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;

        }
    }

    public String mot_passe_oublier(String email) {
        ResultSet resultSet = null;
        try {
            DbConnect j = new DbConnect();

            PreparedStatement pr = j.getCnx().
                    prepareStatement("select password from membre "
                            + "where email=?");

            pr.setString(1, email);

            resultSet = pr.executeQuery();

            if (resultSet.next()) {
                System.out.println(resultSet.getString("password"));
                return email = resultSet.getString("password");

            } else {

                System.out.println("nn");
                return null;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;

        }

    }

    public String mail_existe(String email) {
        ResultSet resultSet = null;
        try {
            DbConnect j = new DbConnect();

            PreparedStatement pr = j.getCnx().
                    prepareStatement("select email from membre "
                            + "where email=?");

            pr.setString(1, email);

            resultSet = pr.executeQuery();

            if (resultSet.next()) {
                return email = resultSet.getString("email");

            } else {

                System.out.println("nn");
                return "adresse email non reconue";
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;

        }

    }

    public  void send_mail(String email) {

        try {
            String host = "smtp.gmail.com";
            String user = "mohamedheidibenkodjha@gmail.com";
            String pass = "nikeluisamere";
            String to = email;
            String from = "mohamedheidibenkodjha@gmail.com";
            String subject = "Rappel du mot de passe";
            String messageText = "Bonjour.\n"
                    + "\n"
                    + "Cher(e) Membre\n"
                    + "\n"
                    + "Voici votre  mot de passe est :" + mot_passe_oublier(email) + "\n"
                    + "\n"
                    + "Vous pouvez desormais vous connecter à votre espace.\n"
                    + "\n"
                    + "Merci";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

             java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(messageText);

            javax.mail.Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            JOptionPane.showMessageDialog(null, "Verifier votre boite mail");
            
        } catch (HeadlessException | MessagingException e) {
            e.getMessage();
            JOptionPane.showMessageDialog(null, "cette adresse e-mail n’est pas associée à un compte ");

        }
    }
    public String message;

    public boolean accepterEmail(String email) {
        boolean ok = false;
        message = "";
        String tex = email;
        int posiArrobase = 0, posiPoint = 0, posi2 = 0;

        if (tex.indexOf(" ") > -1) {                          // signaler l'espace
            message = " il y a un blanc dans l''adresse email ";
        }
        if (message.length() == 0) {
            posiArrobase = tex.indexOf("@");
            if (posiArrobase < 0) {
                message = " arrobase (@) manque dans l'adresse email ";
            }
            if ((posiArrobase == 0) || (tex.endsWith("@"))) {
                message = " arrobase (@) mal placé dans l'adresse email ";
            }
            if ((posiArrobase > 0) && (posiArrobase < tex.length())) {
                posi2 = tex.indexOf("@", posiArrobase + 1);
                if (posi2 > posiArrobase) {
                    message = " double arrobase (@) dans l'adresse email ";
                }
            }
            if (message.length() == 0) {
                posiPoint = tex.indexOf(".");
                if (posiPoint == -1) {
                    message = " on doit trouver au moins un point dans l'adresse email ";
                }
                if ((posiPoint == 0) || (tex.endsWith("."))) {
                    message = " point mal placé dans l'adresse email ";
                }
            }
            if (message.length() == 0) {
                if (tex.length() == 0) {
                    message = " l'adresse email est vide ";
                }
            }
        }
        if (message.length() == 0) {
            ok = true;
        } else {
            ///JOptionPane.showMessageDialog(null,message);
        }
        return (ok);
    }

}
