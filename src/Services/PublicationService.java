/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Crud.PublicationCrud;
import crud.gestion_de_compte;
import entities.Membre;
import entities.Publication;
import java.io.File;
import java.util.Date;

/**
 *
 * @author Malek
 */
public class PublicationService {
    public static File image;
    public static File document;
    gestion_de_compte m =new gestion_de_compte();
    public void ajouterPublication(String localisation,String description,String path){
        System.out.println("test");
        PublicationCrud publicationCrud =new PublicationCrud();
        Publication p =new Publication();
        p.setContenu(description);
        p.setLocalisation(localisation);
        p.setDate(new Date().toString());
        p.setImage(path);
        Membre m =new Membre();
        m.setId(1);
        p.setMembre(gestion_de_compte.id_membre);
        System.out.println(p.toString());
        publicationCrud.ajouterPublication(p);
    }
    public void modifierPublication(Publication p,String contenu,String path){
        p.setContenu(contenu);
        p.setImage(path);
        PublicationCrud pc=new PublicationCrud();
        pc.modifierPublication(p, p.getId());
    }
   
}
