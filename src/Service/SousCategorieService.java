/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity_ib.SousCategorie;
import InterfaceService.IServiceSousCategorie;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import ibtih.MyConnection;

/**
 *
 * @author dorra
 */
public class SousCategorieService implements IServiceSousCategorie {

    Connection cn = MyConnection.getInstance().getConnection();
    Statement st;
    PreparedStatement pst;
    private ResultSet rs;

    @Override
    public SousCategorie getSousCategorie(int id) {
        try {

            String requete = "select * from sous_categorie where id ='" + id + "';";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {
                SousCategorie a;
                a = new SousCategorie(rs.getInt(1), rs.getString(2), rs.getString(3));
                return a;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
      public SousCategorie getSousCategorieByName(String nom) {
        try {

            String requete = "select * from sous_categorie where nomSousCategorie ='" + nom + "';";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {
                SousCategorie a;
                a = new SousCategorie(rs.getInt(1), rs.getString(2), rs.getString(3));
                return a;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
      
    public List<String> getListSSC(int id)
    { List<String> Listp = new ArrayList<>();
     SousCategorie s = new SousCategorie();
     s=getSousCategorie(id);
     Listp.add(s.getNomSousCategorie());
     return Listp;
     
    }

    @Override
    public List<SousCategorie> getSousCategorie() {
        List<SousCategorie> list = new ArrayList<>();
        Connection cn =  MyConnection.getInstance().getConnection();
        PreparedStatement pt;
        try {
            String sql = "select * from sous_categorie ";
            pt = cn.prepareStatement(sql);
            ResultSet resultSet = pt.executeQuery();
            while (resultSet.next()) {
                SousCategorie sctg = new SousCategorie();
                sctg.setNomSousCategorie(resultSet.getString("nomSousCategorie"));
                sctg.setReferenceSousCategorie(resultSet.getString("ReferenceSousCategorie"));
                list.add(sctg);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    
    
    
    
    @Override
    public List<SousCategorie> getAll() {
      List<SousCategorie> Listp = new ArrayList<>();
        String req = "select id,nomSousCategorie,referenceSousCategorie from sous_categorie ";
        PreparedStatement ps ;
        try{
            System.out.println(req);
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
                SousCategorie p = new SousCategorie(result.getInt(1),result.getString(2),result.getString(3));
                Listp.add(p);
                System.out.println(Listp);
                System.out.println("--------------");
            }
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return Listp;
        
    }
    
     @Override
    public void ajoutersouscategorie(SousCategorie p) {
        String req = null;
        
        try {
            req = "INSERT INTO sous_categorie (nomSousCategorie,referenceSousCategorie,categorie_id) VALUES ('" + p.getNomSousCategorie()+ "','" + p.getReferenceSousCategorie()+ "','" + p.getCategorie() + "');";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            System.out.println(req);
            st = cn.createStatement();
            st.executeUpdate(req);
            System.out.println("Sous Categorie ajouté");
        } catch (SQLException e) {
            System.out.println("error");
        }

    }

    
    
    @Override
    public void supprimerSS(int id) {
        String req = "DELETE FROM sous_categorie where id ='" + id + "';";
        try {
            st = cn.createStatement();
            st.executeUpdate(req);
            System.out.println("DONE");

        } catch (SQLException e) {
            System.out.println("error");
        }
    }

   @Override
    public void modifierSSC(SousCategorie r) {
        //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        //Date d=new Date();
        String req = "UPDATE  sous_categorie SET nomSousCategorie='" + r.getNomSousCategorie()+ "' ,referenceSousCategorie='" + r.getReferenceSousCategorie()+ "' WHERE id='"+ r.getId()+"'";
        try {
            st = cn.createStatement();
            st.executeUpdate(req);

        } catch (SQLException e) {
            System.out.println("error");
        }
    }
}