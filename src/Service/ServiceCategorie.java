/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity_ib.Categorie;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ibtih.MyConnection;
import InterfaceService.IServiceCategorie;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author dorra
 */
public class ServiceCategorie {
    
     Connection cn = MyConnection.getInstance().getConnection();
    Statement st;
    PreparedStatement pst;
    private ResultSet rs;
    
     public Categorie getCategorieByName(String nom) {
        try {

            String requete = "select * from categorie where nomCategorie ='" + nom + "';";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {
                Categorie a;
                a = new Categorie(rs.getInt(1), rs.getString(2), rs.getString(3));
                return a;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
     
  
    public List<Categorie> getAll() {
      List<Categorie> Listp = new ArrayList<>();
        String req = "select id,nomCategorie,referenceCategorie from categorie ";
        PreparedStatement ps ;
        try{
            System.out.println(req);
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
                Categorie p = new Categorie(result.getInt(1),result.getString(2),result.getString(3));
                Listp.add(p);
                System.out.println(Listp);
                System.out.println("--------------");
            }
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return Listp;
        
    }
    public Categorie getCategorie(int id) {
        try {

            String requete = "select * from categorie where id ='" + id + "';";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {
                Categorie a;
                a = new Categorie(rs.getInt(1), rs.getString(2), rs.getString(3));
                return a;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
     
    
    public List<Categorie> getCategorie() {
        List<Categorie> list = new ArrayList<>();
        Connection cn =  MyConnection.getInstance().getConnection();
        PreparedStatement pt;
        try {
            String sql = "select * from categorie ";
            pt = cn.prepareStatement(sql);
            ResultSet resultSet = pt.executeQuery();
            while (resultSet.next()) {
                Categorie sctg = new Categorie();
                sctg.setNomCategorie(resultSet.getString("nomCategorie"));
                sctg.setReferenceCategorie(resultSet.getString("ReferenceCategorie"));
                list.add(sctg);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
     
     
     
     
    //@Override
    public void ajoutercategorie(Categorie p) {
        String req = null;
        
        try {
            req = "INSERT INTO categorie (nomCategorie,referenceCategorie) VALUES ('" + p.getNomCategorie()+ "','" + p.getReferenceCategorie()+ "');";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            System.out.println(req);
            st = cn.createStatement();
            st.executeUpdate(req);
            System.out.println(" Categorie ajouté");
        } catch (SQLException e) {
            System.out.println("error");
        }

    }
  
     
     // @Override
    public void supprimerCat(int id) {
        String req = "DELETE FROM categorie where id ='" + id + "';";
        try {
            st = cn.createStatement();
            st.executeUpdate(req);
            System.out.println("DONE");

        } catch (SQLException e) {
            System.out.println("error");
        }
    }
     
      //@Override
    public void modifierCat(Categorie r) {
        //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        //Date d=new Date();
        String req = "UPDATE  categorie SET nomCategorie='" + r.getNomCategorie()+ "' ,referenceCategorie='" + r.getReferenceCategorie()+ "' WHERE id='"+ r.getId()+"'";
        try {
            st = cn.createStatement();
            st.executeUpdate(req);

        } catch (SQLException e) {
            System.out.println("error");
        }
    }
     
}
