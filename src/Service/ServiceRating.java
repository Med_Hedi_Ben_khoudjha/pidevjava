/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity_ib.Categorie;
import Entity_ib.Rating;
import InterfaceService.IServiceRating;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ibtih.MyConnection;

/**
 *
 * @author dorra
 */
public class ServiceRating implements IServiceRating{
    Connection cn = MyConnection.getInstance().getConnection();
    Statement st;
    PreparedStatement pst;
    private ResultSet rs;
    
    @Override
    public ArrayList<Rating> GetAllByProduct(int userId, int productId) {
        try {
            ArrayList<Rating> Listp = new ArrayList<>();
            String requete = "select id, Produit_id, User_id from rating where Produit_id = ? and User_id = ?";
            pst = cn.prepareStatement(requete);
            pst.setInt(1, productId);
            pst.setInt(2, userId);
            rs = pst.executeQuery();
            while (rs.next()) {
                Rating r;
                r = new Rating(rs.getInt(1), rs.getInt(2), rs.getInt(3));
                Listp.add(r);
            }
            return Listp;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void Rate(int prod_id, int user_id, float val) {
        String req = null;
        
        PreparedStatement ps ;
        try {
            req = "INSERT INTO rating(Produit_id,User_id, average, total_count, count) VALUES (?,?,?, 0, 0)";
            ps=cn.prepareStatement(req);
            ps.setInt(1, prod_id);
            ps.setInt(2, user_id);
            ps.setFloat(3, val);
            ps.executeUpdate();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
}
