/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity_ib.Produit;
import Entity_ib.Session;
import Entity_ib.SousCategorie;
import InterfaceService.IServiceProduit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import ibtih.MyConnection;

/**
 *
 * @author dorra
 */
public class ServiceProduit implements IServiceProduit {

    Connection cn = MyConnection.getInstance().getConnection();
    Statement st;
    PreparedStatement pst;
    private ResultSet rs;

    @Override
    public Produit findById(int id_produit) throws SQLException {
        {
            Produit a = new Produit();
            String requete = "select * from produit where id ='" + id_produit + "';";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {
                a = new Produit(rs.getInt(1), rs.getString(2));

            }

            return a;
        }
    }

    public SousCategorie getSousCategorie(int id) {
        try {

            String requete = "select * from sous_categorie where id ='" + id + "';";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {
                SousCategorie a;
                a = new SousCategorie(rs.getInt(1), rs.getString(2), rs.getString(3));
                return a;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public List<String> getPhotos(int id) {
        try {

            String requete = "select PhotoProduit from produit;";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {

                List<String> a = new ArrayList<>(); 
                a.add(rs.getString(requete));
                return a;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
     public List<Integer> getSCategorie() {
        try {

            String requete = "select id from produit;";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {

                List<Integer> a = new ArrayList<>(); 
                a.add(rs.getInt(requete));
                return a;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    
    
    
    
    
    

    public List<String> getListSSC(int id) {
        List<String> Listp = new ArrayList<>();
        SousCategorie s = new SousCategorie();
        s = getSousCategorie(id);
        Listp.add(s.getNomSousCategorie());
        return Listp;

    }

    @Override
    public List<Produit> getAll() {
        List<Produit> Listp = new ArrayList<>();
        String req = "select * from produit ";
        PreparedStatement ps;
        try {
            ps = cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                Produit p = new Produit(result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                       result.getInt(4),
                        result.getFloat(5), 
                        result.getInt(6), 
                        result.getString(7),
                        result.getString(8));
                Listp.add(p);
                System.out.println(Listp);
                System.out.println("--------------");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Listp;

    }

    @Override
    public void ajouterproduit(Produit p) {
        String req = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date d = new Date();

        try {
            req = "INSERT INTO produit(nomProduit,reference,prixVente,prixAchat,quantite,photoProduit,dateProduit,Souscategorie_id) VALUES ('" + p.getNomProduit() + "','" + p.getReference() + "','" + p.getPrixVente() + "','" + p.getPrixAchat() + "','" + p.getQuantite() + "','" + p.getPhotoProduit() + "','" + dateFormat.format(d) + "','" + p.getSousCatg() + "'); ";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            System.out.println(req);
            st = cn.createStatement();
            st.executeUpdate(req);
            System.out.println("produit ajouté");
        } catch (SQLException e) {
            System.out.println("error");
        }

    }

    @Override
    public void supprimerProduit(int id) {
        String req = "DELETE FROM produit where id ='" + id + "';";
        try {
            st = cn.createStatement();
            st.executeUpdate(req);
            System.out.println("DONE");

        } catch (SQLException e) {
            System.out.println("error");
        }
    }

    @Override
    public void modifierProduit(Produit r) {
        //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        //Date d=new Date();
        String req = "UPDATE  produit SET nomProduit='" + r.getNomProduit() + "' ,prixVente='" + r.getPrixVente() + "',prixAchat='" + r.getPrixAchat() + "',quantite='" + r.getQuantite() + "' where id ='" + r.getId() + "';";
        try {
            st = cn.createStatement();
            st.executeUpdate(req);

        } catch (SQLException e) {
            System.out.println("error");
        }
    }

    
    public List<Produit> BoxStuff ()
    {   List<Produit> list= new ArrayList<>();
         try {

            String requete = "SELECT produit.nomProduit, produit.photoProduit   from produit";
            pst = cn.prepareStatement(requete);
            rs = pst.executeQuery(requete);
            while (rs.next()) {
                Produit a;
                a = new Produit(rs.getString(1), rs.getString(2));
                list.add(a);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    
    @Override
    public Produit get(String nomProduit) throws SQLException {

        String req = "Select id,nomProduit,prixVente,quantite,photoProduit from produit where nomProduit = ?";
        PreparedStatement st = cn.prepareStatement(req);
        st.setString(1, nomProduit);
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Produit obj = new Produit();
            obj.setId(rs.getInt("id"));
            obj.setNomProduit(rs.getString("nomProduit"));
            obj.setPrixVente(rs.getFloat("prixVente"));
            obj.setQuantite(rs.getInt("quantite"));

            obj.setPhotoProduit(rs.getString("photoProduit"));
            System.out.println("inside get :" + obj.getNomProduit());
            return obj;
        }
        return null;
    }
    
    //@Override
    public Produit getWithCat(String nomProduit) throws SQLException {

        String req = "Select id,nomProduit,prixVente,quantite,photoProduit, Souscategorie_id from produit where nomProduit = ?";
        PreparedStatement st = cn.prepareStatement(req);
        st.setString(1, nomProduit);
      
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Produit obj = new Produit();
            obj.setId(rs.getInt("id"));
            obj.setNomProduit(rs.getString("nomProduit"));
            obj.setPrixVente(rs.getFloat("prixVente"));
            obj.setQuantite(rs.getInt("quantite"));
            obj.setSousCatg(rs.getInt("Souscategorie_id"));
            obj.setPhotoProduit(rs.getString("photoProduit"));
            System.out.println("inside get :" + obj.getNomProduit() + obj.getSousCatg());
            
            return obj;
        }
        return null;
    }
    
     @Override
    public Produit getPhotos(String nomProduit) throws SQLException {

        String req = "Select id,photoProduit from produit where nomProduit = ?";
        PreparedStatement st = cn.prepareStatement(req);
        st.setString(1, nomProduit);
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Produit obj = new Produit();
            obj.setId(rs.getInt("id"));
           

            obj.setPhotoProduit(rs.getString("photoProduit"));
            System.out.println("inside get :" + obj.getNomProduit());
            return obj;
        }
        return null;
    }
    
    
    
   public void Rating(int idProd , String count) throws SQLException
   {
   
        String req = "UPDATE produit Set reference ='"+count+"'  where id ='"+idProd+"' ";
         try {
        PreparedStatement st = cn.prepareStatement(req);
  //      st.setString(1, count);
    //    st.setInt(2, idProd);
         st.executeUpdate(req);

         } catch (SQLException e) {
            System.out.println(e);
 
        }
      
        }
   
   
   
     @Override
    public List<Produit> getAllBySousCategorie() {
        List<Produit> Listp = new ArrayList<>();
        String req = "select id,nomProduit,reference,prixVente,prixAchat,quantite,photoProduit,Souscategorie_id from produit  ";
        PreparedStatement ps;
        try {
            ps = cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                Produit p = new Produit(result.getInt(1), result.getString(2), result.getString(3), result.getFloat(4), result.getFloat(5), result.getInt(6), result.getString(7),result.getInt(8));
                Listp.add(p);
                System.out.println(Listp);
                System.out.println("--------------");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Listp;

    }
    
    
     public List<Produit> getBest() {
        List<Produit> Listp = new ArrayList<>();
        String req = "SELECT * FROM produit WHERE id = (SELECT Produit_id from rating ORDER BY average Desc limit 1) ";
        PreparedStatement ps;
        try {
            ps = cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                Produit p = new Produit(result.getInt(1), result.getString(2), result.getString(3), result.getFloat(4), result.getFloat(5), result.getInt(6), result.getString(7),result.getInt(8));
                Listp.add(p);
                System.out.println(Listp);
                System.out.println("--------------");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Listp;

    }
   
   
   
   
     public List<Produit> rechercherProduit(String x) {
      List<Produit> Listp = new ArrayList<>();
        String req = "select id,nomProduit,reference,prixVente,prixAchat,quantite,photoProduit from produit where nomProduit like '"+x+"%' or quantite like'"+x+"%';";
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
               
                 Produit r = new Produit(result.getInt(1), result.getString(2), result.getString(3), result.getFloat(4), result.getFloat(5), result.getInt(6), result.getString(7));
                Listp.add(r);
                
            }
        }catch (SQLException e)
        { e.printStackTrace();
            }
        
        return Listp;
        
    }
   
   
   
   
   
   
   
   
   
   
   
   

   }
    
    
    
    
    

