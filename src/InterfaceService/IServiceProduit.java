/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceService;

import Entity_ib.Produit;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Mohamed Bousselmi
 */
public interface IServiceProduit {
    
     public Produit findById(int id_produit)throws SQLException;
     public List<Produit> getAll() ;
     public void ajouterproduit(Produit p);
     public void supprimerProduit(int id);
     public void modifierProduit(Produit r);
      public Produit get(String titre) throws SQLException ;
      public Produit getPhotos(String nomProduit) throws SQLException;
      public List<Produit> getAllBySousCategorie();
      public List<Integer> getSCategorie();
      // public List<Produit> rechercherProduit(String x);
       
             
    
}
