/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceService;

import Entity_ib.Reclamation;
import Entity_ib.Utilisateur;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Zaineb
 */
public interface IServiceUser {
      public  boolean testMotDePasse(String motDePasseGUI, String motDePasseBD);
      public  List<Utilisateur> getTtUtilisateur() ;
      public Utilisateur login(Utilisateur u);
      public int getLastId();
      public Utilisateur findById(int id_user)throws SQLException;
      public List<Utilisateur> getAll();
      public void adduser(Utilisateur u);
      public void deleteuser(int id);
      public void updateuser(Utilisateur u);
}
