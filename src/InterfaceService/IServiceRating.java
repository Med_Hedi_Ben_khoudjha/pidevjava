/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceService;

import Entity_ib.Rating;
import java.util.List;

/**
 *
 * @author dorra
 */
public interface IServiceRating {
    public List<Rating> GetAllByProduct(int userId, int productId);
    public void Rate(int prod_id, int user_id, float val);
}
