/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceService;

import Entity_ib.Categorie;
import java.util.List;

/**
 *
 * @author dorra
 */
public interface IServiceCategorie {
    public Categorie getCategorie(int id) ;
     public List<Categorie> getAll() ;
     public List<Categorie> getCategorie() ;
      public void ajoutercategorie(Categorie p);
}
