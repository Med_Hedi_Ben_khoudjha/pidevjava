/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceService;

import Entity_ib.SousCategorie;
import java.util.List;

/**
 *
 * @author dorra
 */
public interface IServiceSousCategorie {
    
    public SousCategorie getSousCategorie(int id);
    public List<SousCategorie> getSousCategorie();
    public List<SousCategorie> getAll() ;
    public void ajoutersouscategorie(SousCategorie p);
     public void supprimerSS(int id);
      public void modifierSSC(SousCategorie r);
    
}
