/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import crud.gestion_agence;
import crud.gestion_de_compte;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class Res_hController implements Initializable {

    @FXML
    private ImageView retour;
    @FXML
    private Label btn_exit;
    @FXML
    private JFXDatePicker date_aller;
    @FXML
    private JFXDatePicker date_retour;
    @FXML
    private JFXComboBox origine;
    @FXML
    private JFXComboBox destination;
    @FXML
    private Label prix;

             int prix_v;
                ArrayList <String> select= new ArrayList <String>();

        
    gestion_agence u=new gestion_agence();
   ObservableList<String> list_ind = FXCollections.observableList(select);

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
select.add("TUnis");
select.add("France");
select.add("USA");
select.add("Emirate");
select.add("Russie");


origine.setItems(list_ind);
        destination.setItems(list_ind);
        
     
        
        
        
    }    

    @FXML
    private void retour(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }


    @FXML
    private void Valider(ActionEvent event) throws IOException {
    
    
    
    int f=date_aller.getValue().compareTo(date_retour.getValue());
    
    
    
    Date d = new Date();
    System.out.println(d.toString());
    u.insert_reservation(origine.getSelectionModel().getSelectedItem().toString(),
            destination.getSelectionModel().getSelectedItem().toString(),
            date_aller.getValue().toString(),
            date_retour.getValue().toString(),
            prix.getText(),
            d.toString(), 
            gestion_de_compte.id_membre);
    
    
    Parent root = FXMLLoader.load(getClass().getResource("payer.fxml"));
        main.stage.setScene(new Scene(root));
    
    }
public static int index;
    
    private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
    
    @FXML
    private void quit(MouseEvent event) {
                System.exit(0);

    }

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }

    @FXML
    private void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }
    int x = 0, y = 0;

    
    @FXML
    private void pressed(MouseEvent event) {
         x = (int) event.getSceneX();
        y = (int) event.getSceneY();
    }

    @FXML
    private void get_prix(ActionEvent event) {
       index=new Random().nextInt(5 + 1)  + 680;
        prix.setText(Integer.toString(index));
        
    }
    
}
