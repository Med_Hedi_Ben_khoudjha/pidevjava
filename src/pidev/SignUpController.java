package pidev;

import B.D.DbConnect;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class SignUpController implements Initializable {

    @FXML
    public TextField tf_username;

    @FXML
    public TextField tf_email;

    @FXML
    public PasswordField pf_password;

    double x = 0, y = 0;

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void dragged(MouseEvent event) {

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void login(MouseEvent event) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));

    }

    @FXML
     void signup(MouseEvent event) {
            DbConnect j=new DbConnect();
Connection h=j.getCnx();
        try { 

       String sql = "INSERT INTO membre (pseudo,email,psw) VALUES(?,?,?)";
PreparedStatement statement = h.prepareStatement(sql);
//en spécifiant bien les types SQL cibles
//statement.setInt(1,u.getId());
//statement.setString(1,u.getNom());
//statement.setString(3,u.getPrenom());
//statement.setString(4,u.getAdresse());
statement.setString(2,tf_email.getText().toString());
//statement.setString(6,u.getRole());
statement.setString(3,pf_password.getText().toString());
statement.setString(1,tf_username.getText().toString());
//statement.setBoolean(9,u.isPermis());
//statement.setBoolean(10,u.isMotorisé());

statement.executeUpdate(); 
statement.close();
       } 
       catch (SQLException ex) {
          System.err.println(ex.getMessage());
     }


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
