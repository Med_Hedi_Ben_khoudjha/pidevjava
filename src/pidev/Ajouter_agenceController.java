/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import crud.gestion_agence;
import crud.gestion_de_compte;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class Ajouter_agenceController implements Initializable {

    ObservableList<String> list = FXCollections.observableArrayList("Musée","Café",
            "Hotel", "Restaurant");
    gestion_agence u = new gestion_agence();
//ObservableList<String> list_ind = FXCollections.observableList(u.select_indic());

    @FXML
    private Label btn_exit;
    @FXML
    private JFXTextField jfxtextNom;
    @FXML
    private JFXTextField jfxtextTel;
    @FXML
    private ComboBox jfxComboType;
    @FXML
    private JFXTextField jfxtextIp;
    @FXML
    private JFXTextArea jfxtextDesc;
    @FXML
    private ImageView retour;
    @FXML
   // private JFXComboBox jcomboindi;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        RequiredFieldValidator requiredFieldValidator = new RequiredFieldValidator();
        jfxtextNom.getValidators().add(requiredFieldValidator);
        jfxtextTel.getValidators().add(requiredFieldValidator);
        jfxtextIp.getValidators().add(requiredFieldValidator);
        jfxtextDesc.getValidators().add(requiredFieldValidator);

        RequiredFieldValidator validator = new RequiredFieldValidator();
        jfxtextIp.getValidators().add(validator);
        validator.setMessage("Champs invalide");
        jfxtextIp.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!validate(jfxtextIp.getText())) {
                    jfxtextIp.validate();
                }

            }

        });

        
        requiredFieldValidator.setMessage("Champs vide");

        jfxtextNom.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    jfxtextNom.validate();
                }

            }

        });
        jfxtextTel.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    jfxtextTel.validate();
                }

            }

        });
        jfxtextDesc.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    jfxtextDesc.validate();
                }

            }

        });
        jfxtextIp.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if ((!newValue)&&(!validate(jfxtextIp.getText()))) {
                    jfxtextIp.validate();
                }

            }

        });
        //jcomboindi.setItems("216");
        jfxComboType.setValue("Musée");
        jfxComboType.setItems(list);
        jfxComboType.setValue("Hotel");
        jfxComboType.setItems(list);
        jfxComboType.setValue("Restaurant");
        jfxComboType.setItems(list);
    }

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }

    @FXML
    private void quit(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    private void Valider(ActionEvent event) throws IOException, GeoIp2Exception {
       
             u.givenIP_whenFetchingCity_thenReturnsCityData(jfxtextIp.getText().toString());
          String k=gestion_agence.latitude;
            String h=gestion_agence.longitude;
                        String e=gestion_agence.cityName;
            String t=gestion_agence.state;

            u.ajouter_agence(jfxtextNom.getText(),
                 jfxComboType.getSelectionModel().getSelectedItem().toString(),
               e+ " " + t,
                    jfxtextDesc.getText(),
                    h,
                   k,
                    jfxtextTel.getText(),
                    gestion_de_compte.id_membre);

             JOptionPane.showMessageDialog(null, "Agence ajouter");
             // Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
             //main.stage.setScene(new Scene(root));
        
        }

    
private static final Pattern PATTERN = Pattern.compile(
        "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

public static boolean validate(final String ip) {
    return PATTERN.matcher(ip).matches();
}

    @FXML
    private void dragged(MouseEvent event) {Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }
    int x=0,y=0;

    @FXML
    private void pressed(MouseEvent event) {
        x = (int) event.getSceneX();
        y = (int) event.getSceneY();
    }

    @FXML
    private void reset(ActionEvent event) {
        jfxtextDesc.clear();
        jfxtextIp.clear();
        jfxtextNom.clear();
        jfxtextTel.clear();
    }

    @FXML
    private void retour(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }
}
