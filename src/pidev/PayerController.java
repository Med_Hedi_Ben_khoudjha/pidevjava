/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import crud.gestion_agence;
import crud.gestion_de_compte;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class PayerController implements Initializable {

    @FXML
    private ImageView retour;
    @FXML
    private Label btn_exit;
    @FXML
    private Label prix;
    @FXML
    private JFXTextField carte;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void retour(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }
    gestion_agence u = new gestion_agence();

    @FXML
    private void Valider(ActionEvent event) throws IOException {
        Date d = new Date();
        List<Integer> l = new ArrayList<Integer>();
        l=u.select_id(gestion_de_compte.id_membre);
        
            u.insert_payement(gestion_de_compte.id_membre,
                    d.toString(),Integer.valueOf(Res_hController.index).toString(),
                    l.get(1),carte.getText());
            
        
        JOptionPane.showMessageDialog(null,"Payement effectuer");
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
        
    }

    @FXML
    private void quit(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }

    int x = 0, y = 0;

    
    @FXML
    private void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void pressed(MouseEvent event) {

        x = (int) event.getSceneX();
        y = (int) event.getSceneY();
    }

}
