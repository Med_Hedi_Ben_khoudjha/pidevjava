/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import crud.gestion_de_compte;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class Mot_passeController implements Initializable {

    @FXML
    private TextField jTextFieldEmail;
    @FXML
    private Button envoyer;
    @FXML
    private Label erremail;
    @FXML
    private ImageView retour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    double x = 0, y = 0;

    @FXML
    private void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void pressed(MouseEvent event) {
         x = event.getSceneX();
        y = event.getSceneY();
    }

    

    @FXML
    private void send(MouseEvent event) throws IOException {
         String email = jTextFieldEmail.getText().trim();
        gestion_de_compte m = new gestion_de_compte();
        boolean ok = m.accepterEmail(email);
       String j= m.mail_existe(email);

        if(!ok)
        {
            erremail.setText(m.message);
        }
        else if ((ok)&&(j.equals("adresse email non reconue"))){
            JOptionPane.showMessageDialog(null,j);
                        erremail.setText("");
                        jTextFieldEmail.setText("");

        }else if((ok)&&(j.equals(email))){
            Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        main.stage.setScene(new Scene(root));
            m.send_mail(email);
        }
        System.out.println(ok);
    }

    @FXML
    private void retour(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        main.stage.setScene(new Scene(root));
    }

}
