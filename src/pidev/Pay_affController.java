/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import B.D.DbConnect;
import Entity.agence;
import Entity.payement;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXTextField;
import crud.gestion_agence;
import java.awt.List;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class Pay_affController implements Initializable {
    
  gestion_agence u=new gestion_agence();
public static ObservableList<payement> list = FXCollections.observableArrayList();

    @FXML
    private TableView<payement> tab;
    @FXML
    private TableColumn<payement, String> type_a;
    @FXML
    private TableColumn<payement, String> add_a;
    @FXML
    private TableColumn<payement, String> nom_a;
    @FXML
    private Label btn_exit;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
DbConnect j = new DbConnect();
        try {
            ResultSet rs=j.getCnx().createStatement().executeQuery("select * from payement");
            while(rs.next())
            {
                list.add(new payement(
                        rs.getString("date"), 
                        rs.getString("montant"),
                        rs.getString("num_carte")));
                
        type_a.setCellValueFactory(new PropertyValueFactory<>("date"));
        add_a.setCellValueFactory(new PropertyValueFactory<>("montant"));
        nom_a.setCellValueFactory(new PropertyValueFactory<>("num_carte"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Aff_agenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        tab.setItems(list); 
    }    

    @FXML
    private void retour(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void quit(MouseEvent event) {
                System.exit(0);

    }

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }

    @FXML
    private void imprimer(ActionEvent event) throws FileNotFoundException, DocumentException {
        
        Document document=new Document(PageSize.A4);
        
        document.addAuthor("devdemon");
        document.addTitle("Payement");
        PdfWriter.getInstance(document, new FileOutputStream("Payer.pdf"));
        
        document.open();
        for(int i=0;i<list.size();i++)
        {Paragraph paragraph=new Paragraph(list.get(i).toString());
        document.add(paragraph);
        }
        
        document.close();
    }

    
}
