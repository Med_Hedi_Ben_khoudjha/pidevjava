/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import B.D.DbConnect;
import Entity.agence;
import com.jfoenix.controls.JFXTextField;
import crud.gestion_agence;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import static java.util.Locale.filter;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class Aff_agenceController implements Initializable {
    gestion_agence u=new gestion_agence();
ObservableList<agence> list = FXCollections.observableArrayList();
        FilteredList filter=new FilteredList(list, e->true);

    @FXML
    private TableView<agence> tab;
    @FXML
    private TableColumn<agence, String> type_a;
    @FXML
    private TableColumn<agence, String> add_a;
    @FXML
    private TableColumn<agence, String> nom_a;
    @FXML
    private TableColumn<agence, String> tel_a;
    @FXML
    private Label btn_exit;
    @FXML
    private JFXTextField search;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            DbConnect j = new DbConnect();
        try {
            ResultSet rs=j.getCnx().createStatement().executeQuery("select * from agence");
            while(rs.next()){
                list.add(new agence(
                        rs.getString("type"), 
                        rs.getString("adresse"),
                        rs.getString("nom"),
                        rs.getString("telephone"),new Button()));
                
        type_a.setCellValueFactory(new PropertyValueFactory<>("type"));
        add_a.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        nom_a.setCellValueFactory(new PropertyValueFactory<>("telephone"));
        tel_a.setCellValueFactory(new PropertyValueFactory<>("nom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Aff_agenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        tab.setItems(list);
 
        
    }    

    @FXML
    private void retour(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void quit(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }

    @FXML
    private void search(KeyEvent event) {
        search.textProperty().addListener((observable, oldValue, newValue) -> {

filter.setPredicate((Predicate<? super agence>)agence->{
    if(newValue==null || newValue.isEmpty())
    {
        return true;
    }
    String lowercaseFilter=newValue.toLowerCase();
    if(agence.getNom().contains(newValue)){
        return true;
    }
    else if(agence.getAdresse().toLowerCase().contains(newValue))
    {
        return true;
    }
    else if(agence.getType().toLowerCase().contains(newValue)){
        return true;
    }
    
    return false;
    
});
        
        });
        SortedList<agence>sort=new SortedList<>(filter);
        sort.comparatorProperty().bind(tab.comparatorProperty());
        tab.setItems(sort);
    }
    
}
