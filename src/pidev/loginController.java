/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import B.D.DbConnect;
import com.google.common.util.concurrent.Runnables;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import crud.gestion_agence;
import crud.gestion_de_compte;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.management.Notification;
import javax.swing.JOptionPane;
import org.controlsfx.control.Notifications;

/**
 *
 * @author MED
 */
public class loginController implements Initializable {

    @FXML
    private TextField tf_username;

    @FXML
    private PasswordField pf_password;
    @FXML
    private Label usererr;
    @FXML
    private Label errpsw;
    @FXML
    private Hyperlink mp_oublier;
    @FXML
    private ImageView imagev;

    Stage stage = new Stage();

    @FXML
    void login(MouseEvent event) throws SQLException, IOException {
        String username, password;

        username = tf_username.getText();
        password = pf_password.getText();
        gestion_de_compte c = new gestion_de_compte();
        boolean ok = c.s_authentifier(username, password);
        boolean trouver = c.verifier_pseudo(username);

        if (username.isEmpty()) {
            usererr.setText("Veuillez remplir le champ");

        }
        if (password.isEmpty()) {
            errpsw.setText("Veuillez remplir le champ");

        } else if ((trouver) && (ok == false)) {

            errpsw.setText("Veuillez verifier votre mot passe!!");

        }

        if ((trouver) && (ok)) {
             new msgdialog().run();
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));

        }

    }

    double x = 0, y = 0;

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void dragged(MouseEvent event) {

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void signup(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("signup.fxml"));
        main.stage.setScene(new Scene(root));

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
tf_username.setText("adminuser");
pf_password.setText("20152419{ozCj3DwB7PlghlZ0XsW5eR2WnAza8.0tDDjrkNMHfsc}");
    }

    @FXML
    private void Mp_oublie(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("mot_passe.fxml"));
        main.stage.setScene(new Scene(root));
    }

    class msgdialog extends Thread {

        @Override
        public void run() {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(msgdialog.class.getName()).log(Level.SEVERE, null, ex);
            }
            Image image = new Image(getClass().getResourceAsStream("ok.png"));
            Notifications notification = Notifications.create()
                    .title("")
                    .text("Bienvenu")
                    .graphic(new ImageView(image))
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_CENTER)
                    .onAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            System.out.println("Click");
                        }
                    });
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    notification.show();
                }
            });
        }

    }

}
