/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import crud.gestion_de_compte;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class AccController implements Initializable {

    int x=0,y=0;
    private Text user;
    private Label logout;
    @FXML
    private ImageView map;

Tooltip tt = new Tooltip();
Tooltip im1 = new Tooltip();
Tooltip im2 = new Tooltip();
    @FXML
    private ImageView reclamation;
    @FXML
    private ImageView compte;
    @FXML
    private ImageView produit;
    @FXML
    private ImageView pub;

    /**
     * Initializes the controller class.
     */
    
    @Override
public void initialize(URL url, ResourceBundle rb) {
//tt.setText("Deconnecter");
//logout.setTooltip(tt);
im1.setText("Agence");
Tooltip.install(map, new Tooltip("Agence"));
Tooltip.install(reclamation, new Tooltip("Reclamation"));
Tooltip.install(compte, new Tooltip("Compte"));
Tooltip.install(compte, new Tooltip("Produit"));

    }    


    @FXML
    private void pressed(MouseEvent event) {
        
         x = (int) event.getSceneX();
        y = (int) event.getSceneY();
    }

    @FXML
    private void dragged(MouseEvent event) {
         Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    private void chercheragence(MouseEvent event) throws IOException {
       
         Parent root = FXMLLoader.load(getClass().getResource("aff_agence.fxml"));
        main.stage.setScene(new Scene(root));
    }

   

    @FXML
    private void map(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("M_A.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void reclamationView(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("reclamation.fxml"));
        main.stage.setScene(new Scene(root));


        
    }

    @FXML
    private void offre(MouseEvent event) throws IOException {
    
    
    Parent root = FXMLLoader.load(getClass().getResource("offre.fxml"));
        main.stage.setScene(new Scene(root));
    }

    

    

    @FXML
    private void Inter_cr_agence(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("ajouter_agence.fxml"));
        main.stage.setScene(new Scene(root));

    
    }

    @FXML
    private void deconnecter(ActionEvent event) throws IOException {
    
         Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void ajouter_offre(ActionEvent event) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("ajouter_offre.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void res_vols(MouseEvent event) throws IOException {
     Parent root = FXMLLoader.load(getClass().getResource("res_h.fxml"));
        main.stage.setScene(new Scene(root));   
    }

    @FXML
    private void pdf(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("pay_aff.fxml"));
        main.stage.setScene(new Scene(root));
    }

   

    @FXML
    private void produit(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/gui/Produit.fxml"));
        main.stage.setScene(new Scene(root));
        
    }

  
    
    
    @FXML
    private void pub(MouseEvent event) throws IOException {
    
     Parent root = FXMLLoader.load(getClass().getResource("/mesinterfaces/AddPublication.fxml"));
        main.stage.setScene(new Scene(root));
    }
    

    

    

    

    
}
