/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import B.D.DbConnect;
import Entity.agence;
import Entity.offre;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import crud.gestion_agence;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;

import java.awt.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class OffreController implements Initializable {

    gestion_agence u = new gestion_agence();
    ObservableList<agence> list = FXCollections.observableArrayList();
    FilteredList filter = new FilteredList(list, e -> true);
     List<Integer> bo ;
    @FXML
    private TableView tab;

    @FXML
    private Label btn_exit;
    @FXML
    private JFXTextField search;

    TableColumn type = new TableColumn("Type");
    TableColumn address = new TableColumn("Adresse");
    TableColumn nom = new TableColumn("Nom Agence");
    TableColumn Telephone = new TableColumn("Telephone");
    TableColumn Action = new TableColumn("Action");
    @FXML
    private Label info;
    @FXML
    private JFXButton reserver;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        tab.getColumns().addAll(type, address, nom, Telephone, Action);
     List<Integer> bo = new ArrayList<Integer>();

        DbConnect j = new DbConnect();
        try {
            ResultSet rs = j.getCnx().createStatement().executeQuery("select * from agence");
            while (rs.next()) {
               int  id = rs.getInt("id");
                String button = null;
                Button Button = null;
                list.add(new agence(
                        rs.getString("type"),
                        rs.getString("adresse"),
                        rs.getString("nom"),
                        rs.getString("telephone"), Button));
bo.add(id);
                type.setCellValueFactory(new PropertyValueFactory<>("type"));
                address.setCellValueFactory(new PropertyValueFactory<>("adresse"));
                nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
                Telephone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
                Action.setCellValueFactory(new PropertyValueFactory<>("button"));

//        System.out.println(k.getType().toString()); 
               
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(Aff_agenceController.class.getName()).log(Level.SEVERE, null, ex);
        }

        tab.setItems(list);
         System.out.println();
            for (int i = 0; i < list.size(); i++) {
                    
                    Button f =new Button();
                            f=list.get(i).getButton();
                    info.setText("");

                    f.setOnAction((event) -> {
        System.out.println(tab.getSelectionModel().getSelectedIndex());
                                    offre k=gestion_agence.select_offre(bo.get(tab.getSelectionModel().getSelectedIndex()));                     
                     
                       info.setText(k.toString());
                       System.out.println(k.toString());
                         });
                }

    }

    @FXML
    private void retour(MouseEvent event) throws IOException {
         Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void quit(MouseEvent event) {
        System.exit(0);

    }

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }

    @FXML
    private void search(KeyEvent event) {
        search.textProperty().addListener((observable, oldValue, newValue) -> {

            filter.setPredicate((Predicate<? super agence>) agence -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowercaseFilter = newValue.toLowerCase();
                if (agence.getNom().contains(newValue)) {
                    return true;
                } else if (agence.getAdresse().toLowerCase().contains(newValue)) {
                    return true;
                } else if (agence.getType().toLowerCase().contains(newValue)) {
                    return true;
                }

                return false;

            });

        });
        SortedList<agence> sort = new SortedList<>(filter);
        sort.comparatorProperty().bind(tab.comparatorProperty());
        tab.setItems(sort);
    }

}
