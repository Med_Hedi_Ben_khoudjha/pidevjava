/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import B.D.DbConnect;
import Entity.agence;
import Entity.offre;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import crud.gestion_agence;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class Ajouter_offreController implements Initializable {
ObservableList<String> list_type = FXCollections.observableArrayList("restaurant",
            "Musée", "Hotels","Vols");
    @FXML
    private TableView<agence> tab;
    @FXML
    private Label btn_exit;
    @FXML
    private JFXTextField search;
    @FXML
    private JFXTextArea info;

    
    
    gestion_agence u = new gestion_agence();
    ObservableList<agence> list = FXCollections.observableArrayList();
    
    FilteredList filter = new FilteredList(list, e -> true);
     public static List<Integer> bo ;
    @FXML
    private JFXDatePicker date;
    @FXML
    private JFXComboBox Type;
    @FXML
    private TableColumn<agence, String> type_a;
    @FXML
    private TableColumn<agence, String> add_a;
    @FXML
    private TableColumn<agence, String> nom_a;
    @FXML
    private TableColumn<agence, String> tel_a;
    @FXML
    private JFXButton Valider;
    
    public int index;
    @FXML
    private TableColumn<?, ?> Action;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
     List<Integer> bo = new ArrayList<Integer>();

        DbConnect j = new DbConnect();
        try {
            ResultSet rs = j.getCnx().createStatement().executeQuery("select * from agence");
            while (rs.next()) {
               int  id = rs.getInt("id");
                
                list.add(new agence(
                        rs.getString("type"),
                        rs.getString("adresse"),
                        rs.getString("nom"),
                        rs.getString("telephone"),new Button("Ajouter")));
       bo.add(id);
                type_a.setCellValueFactory(new PropertyValueFactory<>("type"));
                add_a.setCellValueFactory(new PropertyValueFactory<>("adresse"));
                nom_a.setCellValueFactory(new PropertyValueFactory<>("nom"));
                tel_a.setCellValueFactory(new PropertyValueFactory<>("telephone"));
                tel_a.setCellValueFactory(new PropertyValueFactory<>("telephone"));
                Action.setCellValueFactory(new PropertyValueFactory<>("button"));

               
            }
           rs.close();
           
        } catch (SQLException ex) {
            Logger.getLogger(Aff_agenceController.class.getName()).log(Level.SEVERE, null, ex);
        }

            
        Type.setValue("Restaurant");
        Type.setItems(list_type);
        Type.setValue("Vols");
        Type.setItems(list_type);

        Type.setValue("Hotels");
        Type.setItems(list_type);
        Type.setValue("Musée");
        Type.setItems(list_type);
                tab.setItems(list);

for (int i = 0; i < bo.size(); i++) {
                    
                    Button f =new Button();
                            f=list.get(i).getButton();
                    info.setText("");

                    f.setOnAction((event) -> {
        System.out.println(tab.getSelectionModel().getSelectedIndex());
        u.insert_offre(Type.getSelectionModel().getSelectedItem().toString(), info.getText(), date.getValue().toString(),bo.get(tab.getSelectionModel().getSelectedIndex()));
                     
                         });
                }

    }    

    
    
    @FXML
    private void retour(MouseEvent event) throws IOException {
         Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void quit(MouseEvent event) {
    System.exit(0);
    }

    @FXML
    private void handleButtonAction(MouseEvent event) {
    }

    @FXML
    private void search(KeyEvent event) {
        
        
        search.textProperty().addListener((observable, oldValue, newValue) -> {

            filter.setPredicate((Predicate<? super agence>) agence -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowercaseFilter = newValue.toLowerCase();
                if (agence.getNom().contains(newValue)) {
                    return true;
                } else if (agence.getAdresse().toLowerCase().contains(newValue)) {
                    return true;
                } else if (agence.getType().toLowerCase().contains(newValue)) {
                    return true;
                }

                return false;

            });

        });
        SortedList<agence> sort = new SortedList<>(filter);
        sort.comparatorProperty().bind(tab.comparatorProperty());
        tab.setItems(sort);
    }

    @FXML
    private void insert(ActionEvent event) {
        
        //System.out.println(index_tab());
        
        u.insert_offre(Type.getSelectionModel().getSelectedItem().toString(), info.getText(), date.toString(),index);
        
        
    }

   

    @FXML
    private void ind(MouseEvent event) {
                

    }
    
}
