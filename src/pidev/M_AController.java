package pidev;

import Entity.agence;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.JavascriptObject;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.service.directions.DirectionsRequest;
import com.lynden.gmapsfx.service.directions.TravelModes;
import com.lynden.gmapsfx.service.geocoding.GeocoderStatus;
import com.lynden.gmapsfx.service.geocoding.GeocodingResult;
import com.lynden.gmapsfx.service.geocoding.GeocodingService;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.service.geocoding.GeocodingServiceCallback;
import crud.gestion_agence;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Emir
 */
public class M_AController implements Initializable, MapComponentInitializedListener {

    gestion_agence f = new gestion_agence();
    agence b;

    @FXML
    private GoogleMapView mapView;

    private StringProperty address = new SimpleStringProperty();
    private GeocodingService geocodingService;
    private GoogleMap googlemap;

    String adresseEvent = "";
    private float longitude;
    private float latitude;
    LatLong latLong = null;
    String path = "";
    @FXML
    private ImageView retour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mapView.addMapInializedListener(this);

    }
    List<agence> l = new ArrayList<>();

    @Override
    public void mapInitialized() {

        // mapView=new GoogleMapView("en", "AIzaSyBWeRU02YUYPdwRuMFyTKIXUbHjq6e35Gw");
        geocodingService = new GeocodingService();
        GeocodingServiceCallback callback = geocodingService.callback;
        //System.out.println(callback);
        MapOptions options = new MapOptions();

        options.center(new LatLong(47.606189, -122.335842))
                .zoomControl(true)
                .zoom(12).rotateControl(false)
                .overviewMapControl(false).mapMaker(true)
                .mapType(MapTypeIdEnum.ROADMAP);

        googlemap = mapView.createMap(options);
        agence b=null;
        l = f.select();
        for (int i = 0; i < l.size(); i++) {
             b = l.get(i);
            MarkerOptions markerOptions = new MarkerOptions();
            LatLong markerLatLong = new LatLong(Double.valueOf(b.getLongitude()), Double.valueOf(b.getLatitude()));
            markerOptions.position(markerLatLong)
                    .title("My new Marker").animation(Animation.BOUNCE)
                    .visible(true);

            Marker myMarker = new Marker(markerOptions);

            googlemap.addMarker(myMarker);
            InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
            infoWindowOptions.content(b.getType() + "<br>"
                    + b.getNom() + "<br>"
                    + b.getAdresse() + "<br>"
                    +"Teléphone :"+ b.getTelephone());

            
            InfoWindow fredWilkeInfoWindow = new InfoWindow(infoWindowOptions);
            fredWilkeInfoWindow.open(googlemap, myMarker);
        
        }
        

    }

    public void adress(String adresse) {

        geocodingService.geocode(adresse, new GeocodingServiceCallback() {
            @Override
            public void geocodedResultsReceived(GeocodingResult[] results, GeocoderStatus status) {
                LatLong latLong = null;

                if (status == GeocoderStatus.ZERO_RESULTS) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "No matching address found");
                    alert.show();
                    return;
                } else if (results.length > 1) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "Multiple results found, showing the first one.");
                    alert.show();
                    latLong = new LatLong(results[0].getGeometry().getLocation().getLatitude(), results[0].getGeometry().getLocation().getLongitude());
                    latitude = (float) results[0].getGeometry().getLocation().getLatitude();
                    longitude = (float) results[0].getGeometry().getLocation().getLongitude();
                } else {
                    latLong = new LatLong(results[0].getGeometry().getLocation().getLatitude(), results[0].getGeometry().getLocation().getLongitude());
                    latitude = (float) results[0].getGeometry().getLocation().getLatitude();
                    longitude = (float) results[0].getGeometry().getLocation().getLongitude();
                }
                System.out.println(latLong);
                Marker marker = new Marker(new MarkerOptions().position(latLong));

                googlemap.addMarker(marker);
            }
        });

    }

    private void addressTextFieldAction(ActionEvent event) {
//        geocodingService.geocode(address.get(), (GeocodingResult[] results, GeocoderStatus status) -> {
//            
//            LatLong latLong = null;
//            
//            if( status == GeocoderStatus.ZERO_RESULTS) {
//                Alert alert = new Alert(Alert.AlertType.ERROR, "No matching address found");
//                alert.show();
//                return;
//            } else if( results.length > 1 ) {
//                Alert alert = new Alert(Alert.AlertType.WARNING, "Multiple results found, showing the first one.");
//                alert.show();
//                latLong = new LatLong(results[0].getGeometry().getLocation().getLatitude(), results[0].getGeometry().getLocation().getLongitude());
//            } else {
//                latLong = new LatLong(results[0].getGeometry().getLocation().getLatitude(), results[0].getGeometry().getLocation().getLongitude());
//            }
//            
//            googlemap.setCenter(latLong);
//
//        });
        locate();

    }

    private void locate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @FXML
    private void localisation(MouseEvent event) {
    }

    @FXML
    private void retour(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }

}
