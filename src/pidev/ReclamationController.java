/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.validation.RequiredFieldValidator;
import crud.gestion_agence;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class ReclamationController implements Initializable {

    gestion_agence h = new gestion_agence();
    ObservableList<String> list = FXCollections.observableArrayList("Reclamation probleme de connexion",
            "Reclamation hotel non conforme", "Reclamation vol annulé");

    @FXML
    private ImageView retour;
    @FXML
    private Button envoyer;
    @FXML
    private JFXTextArea text;

    static String continu;
    static String jcontinu;
    @FXML
    private JFXComboBox JCombobox;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                RequiredFieldValidator requiredFieldValidator = new RequiredFieldValidator();
        text.getValidators().add(requiredFieldValidator);
        requiredFieldValidator.setMessage("Champs vide");
        text.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    text.validate();
                }

            }

        });

        JCombobox.setValue("Reclamation probleme de connexion");
        JCombobox.setItems(list);
        JCombobox.setValue("Reclamation hotel non conforme");
        JCombobox.setItems(list);

        JCombobox.setValue("Reclamation vol annulé");
        JCombobox.setItems(list);

    }

    @FXML
    private void retour(MouseEvent event) throws IOException {
         Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }
    int x = 0, y = 0;

    @FXML
    private void pressed(MouseEvent event) {
        x = (int) event.getSceneX();
        y = (int) event.getSceneY();
    }

    @FXML
    private void send(MouseEvent event) throws IOException {
        String continu = text.getText();
        String jcontinu = JCombobox.getSelectionModel().getSelectedItem().toString();
        h.reclamation(continu, jcontinu);
        JOptionPane.showMessageDialog(null, "Reclamation envoyer");
        Parent root = FXMLLoader.load(getClass().getResource("acc.fxml"));
        main.stage.setScene(new Scene(root));
    }

    @FXML
    private void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

}
