/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package B.D;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author MED
 */
public class DbConnect {
    String url="jdbc:mysql://localhost:3306/pidev";
    String login="root";
    String pwd="";
    static DbConnect myConnection;
    public DbConnect() {
    }
    
    public static DbConnect getmyconnection(){
        if(myConnection==null){
            myConnection=new DbConnect();
        }
        return myConnection;
    }
    
    public Connection cnx;
    public void conn(){
        
        try {
            cnx=DriverManager.getConnection(url, login, pwd);
            System.out.println("Connexion etablie");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public Connection getCnx() {
        try {
            return  cnx=DriverManager.getConnection(url, login, pwd);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;

    }
    
}
