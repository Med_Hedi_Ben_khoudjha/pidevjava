/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import pidev.OffreController;

/**
 *
 * @author MED
 */
public class agence {
    private int id;
    private String type;
    private String adresse;
    private String ip;
    private String telephone;
    private String nom;
    private String longitude;
    private String latitude;
    private Button button;

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }
    public agence() {
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public agence(int id, String type, String adresse, String ip, String telephone, String nom, String longitude, String latitude) {
        this.id = id;
        this.type = type;
        this.adresse = adresse;
        this.ip = ip;
        this.telephone = telephone;
        this.nom = nom;
        this.longitude = longitude;
        this.latitude = latitude;
    }
public agence(String type, String adresse, String telephone, String nom,Button button) {
        this.type = type;
        this.adresse = adresse;
        this.telephone = telephone;
        this.nom = nom;
        this.button=new Button("Consulter");
    }

public agence(String type, String adresse, String telephone, String nom) {
        this.type = type;
        this.adresse = adresse;
        this.telephone = telephone;
        this.nom = nom;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }
   
    
    

    public String getAdresse() {
        return adresse;
    }

    public String getIp() {
        return ip;
    }

    public int getId() {
        return id;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getType() {
        return type;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setIp(String avis) {
        this.ip = ip;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setType(String type) {
        this.type = type;
    }

     
    
}
