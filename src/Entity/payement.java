/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author MED
 */
public class payement {
    private  String date;
    private String montant;
    private String num_carte;

    public payement(String date, String montant, String num_carte) {
        this.date = date;
        this.montant = montant;
        this.num_carte = num_carte;
    }

    public String getDate() {
        return date;
    }

    public String getNum_carte() {
        return num_carte;
    }

    public String getMontant() {
        return montant;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public void setNum_carte(String num_carte) {
        this.num_carte = num_carte;
    }

    @Override
    public String toString() {
        return "Date : "+date+"\n"
                +"Montant : "+montant+"\n"
                +"Numero carte :"+num_carte+"\n"; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
