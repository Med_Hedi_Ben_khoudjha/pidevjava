/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity_ib;

/**
 *
 * @author dorra
 */
public class Rating {
    private int id;
    private float average;
    private float total_count;
    private int count;
    private int Produit_id;
    private int User_id;

    public Rating(int id, int Produit_id, int User_id) {
        this.id = id;
        this.Produit_id = Produit_id;
        this.User_id = User_id;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public float getTotal_count() {
        return total_count;
    }

    public void setTotal_count(float total_count) {
        this.total_count = total_count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getProduit_id() {
        return Produit_id;
    }

    public void setProduit_id(int Produit_id) {
        this.Produit_id = Produit_id;
    }

    public int getUser_id() {
        return User_id;
    }

    public void setUser_id(int User_id) {
        this.User_id = User_id;
    }
    
    
}
