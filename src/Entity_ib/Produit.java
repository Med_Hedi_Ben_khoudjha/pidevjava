/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity_ib;

import java.sql.Date;

/**
 *
 * @author Mohamed Bousselmi
 */
public class Produit {

    private int id;
    private String nomProduit;
    private String photoProduit;
    private String dateProduit;
    private String reference;

   
    private float prixVente;
    private float prixAchat;
    private int quantite;
    private int sousCatg;

    public Produit(int id, String nomProduit, String reference, float prixVente, float prixAchat, int quantite,  String photoProduit, int sousCatg) {
        this.id = id;
        this.nomProduit = nomProduit;
        this.photoProduit = photoProduit;
       
        this.reference = reference;
        this.prixVente = prixVente;
        this.prixAchat = prixAchat;
        this.quantite = quantite;
        this.sousCatg = sousCatg;
    }
    
    
    public Produit(String nomProduit , int quantite) 
    {
    this.nomProduit = nomProduit;
    this.quantite = quantite;
    }

    public Produit() {
    }
 public Produit(String photoProduit) {
        this.photoProduit = photoProduit;
    }
    public Produit(int id, String nomProduit) {
        this.id = id;
        this.nomProduit = nomProduit;
    }

    public Produit(String nomProduit, String reference, float prixVente, float prixAchat, int quantite, String photoProduit) {
        this.nomProduit = nomProduit;
        this.photoProduit = photoProduit;
        this.reference = reference;
        this.prixVente = prixVente;
        this.prixAchat = prixAchat;
        this.quantite = quantite;
    }

    public Produit(String nomProduit, String reference, float prixVente, float prixAchat, int quantite, String photoProduit, String dateProduit) {
        this.nomProduit = nomProduit;
        this.photoProduit = photoProduit;
        this.dateProduit = dateProduit;
        this.reference = reference;
        this.prixVente = prixVente;
        this.prixAchat = prixAchat;
        this.quantite = quantite;
        // this.sousCatg = sousCatg;
    }

    public Produit(int id, String nomProduit, String reference, float prixVente, float prixAchat, int quantite, String photoProduit) {
        this.id = id;
        this.nomProduit = nomProduit;
        this.photoProduit = photoProduit;

        this.reference = reference;
        this.prixVente = prixVente;
        this.prixAchat = prixAchat;
        this.quantite = quantite;

    }
    
      public Produit(int id, String nomProduit, String reference, float prixVente, float prixAchat, int quantite, String photoProduit, String dateProduit) {
        this.id = id;
        this.nomProduit = nomProduit;
        this.photoProduit = photoProduit;
 this.dateProduit = dateProduit;
        this.reference = reference;
        this.prixVente = prixVente;
        this.prixAchat = prixAchat;
        this.quantite = quantite;

    }
    
     public Produit(String nomProduit,String photoProduit) {
      
        this.nomProduit = nomProduit;
        this.photoProduit = photoProduit; }

    public Produit(String nomProduit, String reference, float prixVente, float prixAchat, int quantite, String photoProduit, String dateProduit, int sousCatg) {
        this.nomProduit = nomProduit;
        this.photoProduit = photoProduit;
        this.dateProduit = dateProduit;
        this.reference = reference;
        this.prixVente = prixVente;
        this.prixAchat = prixAchat;
        this.quantite = quantite;
        this.sousCatg = sousCatg;
    }

    public int getSousCatg() {
        return this.sousCatg;
    }

    public void setSousCatg(int sousCatg) {
        this.sousCatg = sousCatg;
    }

    public Produit(int id, String nomProduit, float prixVente, float prixAchat, int quantite) {
        this.id = id;
        this.nomProduit = nomProduit;
        this.prixVente = prixVente;
        this.prixAchat = prixAchat;
        this.quantite = quantite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public String getPhotoProduit() {
        return photoProduit;
    }

    public void setPhotoProduit(String photoProduit) {
        this.photoProduit = photoProduit;
    }

    public String getDateProduit() {
        return dateProduit;
    }

    public void setDateProduit(String dateProduit) {
        this.dateProduit = dateProduit;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public float getPrixVente() {
        return prixVente;
    }

    public void setPrixVente(float prixVente) {
        this.prixVente = prixVente;
    }

    public float getPrixAchat() {
        return prixAchat;
    }

    public void setPrixAchat(float prixAchat) {
        this.prixAchat = prixAchat;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return "" + nomProduit + "," + reference + "," + prixVente + "," + prixAchat + "," + quantite + "," + photoProduit + "," + dateProduit + "";
    }

}
