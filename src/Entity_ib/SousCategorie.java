/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity_ib;

/**
 *
 * @author dorra
 */
public class SousCategorie {

    private int id;
    private String nomSousCategorie;
    private String referenceSousCategorie;
    private int categorie;

    public SousCategorie(int id, String nomSousCategorie, String referenceSousCategorie, int categorie) {
        this.id = id;
        this.nomSousCategorie = nomSousCategorie;
        this.referenceSousCategorie = referenceSousCategorie;
        this.categorie = categorie;
    }

    public SousCategorie(String nomSousCategorie, String referenceSousCategorie, int categorie) {
        this.nomSousCategorie = nomSousCategorie;
        this.referenceSousCategorie = referenceSousCategorie;
        this.categorie = categorie;
    }

    public int getCategorie() {
        return categorie;
    }

    public void setCategorie(int categorie) {
        this.categorie = categorie;
    }

    public SousCategorie() {
    }

    public SousCategorie(int id, String nomSousCategorie, String referenceSousCategorie) {
        this.id = id;
        this.nomSousCategorie = nomSousCategorie;
        this.referenceSousCategorie = referenceSousCategorie;
    }

    public SousCategorie(String nomSousCategorie, String referenceSousCategorie) {
         this.nomSousCategorie = nomSousCategorie;
        this.referenceSousCategorie = referenceSousCategorie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomSousCategorie() {
        return nomSousCategorie;
    }

    public void setNomSousCategorie(String nomSousCategorie) {
        this.nomSousCategorie = nomSousCategorie;
    }

    public String getReferenceSousCategorie() {
        return referenceSousCategorie;
    }

    public void setReferenceSousCategorie(String referenceSousCategorie) {
        this.referenceSousCategorie = referenceSousCategorie;
    }

    public SousCategorie(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return nomSousCategorie ;
    }

}
