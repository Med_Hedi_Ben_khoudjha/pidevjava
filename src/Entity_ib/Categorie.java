/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity_ib;

/**
 *
 * @author dorra
 */
public class Categorie {
     private int id;
    private String nomCategorie;
    private String referenceCategorie;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Categorie{" + "id=" + id + ", nomCategorie=" + nomCategorie + ", referenceCategorie=" + referenceCategorie + '}';
    }

    public String getNomCategorie() {
        return nomCategorie;
    }

    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }

    public String getReferenceCategorie() {
        return referenceCategorie;
    }

    public void setReferenceCategorie(String referenceCategorie) {
        this.referenceCategorie = referenceCategorie;
    }

    public Categorie() {
    }

    public Categorie(String nomCategorie, String referenceCategorie) {
        this.nomCategorie = nomCategorie;
        this.referenceCategorie = referenceCategorie;
    }

    public Categorie(int id, String nomCategorie, String referenceCategorie) {
        this.id = id;
        this.nomCategorie = nomCategorie;
        this.referenceCategorie = referenceCategorie;
    }
    
}
