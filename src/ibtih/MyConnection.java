/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ibtih;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author Mohamed Bousselmi
 */

public class MyConnection {

   private String url="jdbc:mysql://localhost:3306/pidev";
    private String login ="root";
    private String pwd="";
    private Connection cnx ; 
    private static MyConnection instance;
    private MyConnection()
    {
        try{
              
            cnx = DriverManager.getConnection(url,login,pwd);
            Statement ste=cnx.createStatement();

           // System.out.println("connexion etablie");
             //String afficherpersonne="SELECT * FROM `user`";
            //ResultSet res = ste.executeQuery(afficherpersonne);
            //while (res.next()) {
              //  System.out.println(res.getInt(1)+" "+res.getString(2)+" "+res.getString(3)+" "+res.getString(4)+res.getString(5));  
                
            //}
            
        }catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public Connection getConnection()
    {
        return cnx;
    }
    
    public static MyConnection getInstance()
    {
        if(instance==null){
            instance = new MyConnection();
        }
        return instance ; 
    }
}
