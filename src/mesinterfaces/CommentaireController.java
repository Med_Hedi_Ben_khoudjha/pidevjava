/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinterfaces;

import Services.CommentaireService;
import Services.PublicationService;
import entities.Membre;
import entities.Publication;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import utils.BadWords;

/**
 * FXML Controller class
 *
 * @author Malek
 */
public class CommentaireController implements Initializable {
    @FXML
    private AnchorPane app;
    @FXML
    private TextArea commentaire;
    @FXML
    private Button ajouterC;
    
    File outputfile;
    @FXML
    private Button Annuler;
    
    Stage stage;
    Scene scene;
    @FXML
    private Button ListeCom;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void AjouterCommentaire(ActionEvent event) {
        
        
        
        
          try {
             CommentaireService ps = new CommentaireService() ;
                 Membre m=new Membre();
                 m.setId(1);
                Publication p=new Publication();
                p.setId(23);
            BadWords.loadConfigs();
            if (outputfile != null) {
                if (BadWords.filterText(commentaire.getText())) {
                    
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("gros mot");
                    alert.setHeaderText("gros mot trouve");
                    alert.setContentText("cette application n'autorise pas ces termes ");
                    alert.showAndWait();
                } else {
                    ps.AjouterCommentaire(commentaire.getText(),p);
                }
               // ImageIO.write(in, "png", outputfile);

            } else {
                if (BadWords.filterText(commentaire.getText())) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("gros mot");
                    alert.setHeaderText("gros mot trouve");
                    alert.setContentText("cette application n'autorise pas ces termes ");
                    alert.showAndWait();
                } else {
                    ps.AjouterCommentaire(commentaire.getText(),p  );
                }
            }
               } catch (Exception ex) {
            Logger.getLogger(ModifierPublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    /*    
        
        CommentaireService ps = new CommentaireService() ;
        Membre m=new Membre();
        m.setId(1);
        Publication p=new Publication();
        p.setId(23);
        
        ps.AjouterCommentaire(commentaire.getText(),p ,m );
    }
   */ 
}

    @FXML
    private void AnnulerR(ActionEvent event) {
        Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/listePublication.fxml")), 765, 573);
        } catch (IOException ex) {
            Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void ListeComm(ActionEvent event) {
          Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ListeCommentaire.fxml")), 726, 495);
        } catch (IOException ex) {
            Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
        
    }
}
