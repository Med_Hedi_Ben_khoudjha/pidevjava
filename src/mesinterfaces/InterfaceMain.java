/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinterfaces;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Malek
 */
public class InterfaceMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws IOException{
        Parent root= FXMLLoader.load(getClass()
                               .getResource("AddPublication.fxml"));
        Scene scene=new Scene(root);
        primaryStage.setTitle("hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
    }
    
}
