package mesinterfaces;

import Services.PublicationService;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import static mesinterfaces.ModifierPublicationController.in;
//import static mesinterfaces.ModifierPublicationController.pub;
import utils.BadWords;

public class AddPublicationController implements Initializable {

    @FXML
    private AnchorPane ap;

    @FXML
    private TextArea TextA;

    @FXML
    private TextField TextF;

    @FXML
    private Button publierButton;

    @FXML
    private FontAwesomeIcon imageChooser;

    @FXML
    private FontAwesomeIcon publier;

    File outputfile;
    File f;
    @FXML
    private Button Listepub;
    Stage stage;
    Scene scene;

    void publierAction(ActionEvent event) {

      
      
       /* try {
            PublicationService ps = new PublicationService();
            BadWords.loadConfigs();
            if (f != null) {
                if (BadWords.filterText(TextA.getText())) {

                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("gros mot");
                    alert.setHeaderText("gros mot trouve");
                    alert.setContentText("cette application n'autorise pas ces termes ");
                    alert.showAndWait();
                } else {
                    ps.ajouterPublication(TextA.getText(), TextF.getText(), outputfile.getName());
                }
                ImageIO.write(in, "png", outputfile);

            } else {
                if (BadWords.filterText(TextA.getText())) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("gros mot");
                    alert.setHeaderText("gros mot trouve");
                    alert.setContentText("cette application n'autorise pas ces termes ");
                    alert.showAndWait();
                } else {
                    ps.ajouterPublication(TextA.getText(), TextF.getText(), outputfile.getName());
                }
            }
            Node node = (Node) event.getSource();
            stage = (Stage) node.getScene().getWindow();

            try {
                scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/listePublication.fxml")), 765, 573);
            } catch (IOException ex) {
                Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            stage.setScene(scene);
            stage.show();

        } catch (Exception ex) {
            Logger.getLogger(ModifierPublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
    }

    final FileChooser fileChooser = new FileChooser();
    static File file;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imageChooser.setOnMouseClicked(new EventHandler<Event>() {

            public void handle(Event event) {
                configureFileChooser(fileChooser);
                file = fileChooser.showOpenDialog((Stage) ap.getScene().getWindow());
                if (file != null) {
                    try {
                        BufferedImage in = ImageIO.read(file);
                        outputfile = new File("src/image/", file.getName());
                        ImageIO.write(in, "png", outputfile);

                    } catch (IOException ex) {
                        Logger.getLogger(AddPublicationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                    //setFile(file);

            }
        });
        publier.setOnMouseClicked((Event event) -> {
            //configureFileChooser(fileChooser);
            file = fileChooser.showOpenDialog((Stage) ap.getScene().getWindow());
            if (file != null) //setFile(file);
            {
                System.out.println(file.getName());
            }
        });
    }

    private static void configureFileChooser(final FileChooser fileChooser) {
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

    }
    /* private void fileSave(File f){
     System.out.println("dkhalt2");
     Path currentDir = Paths.get("C:\\test\\");
     try {
     Files.copy(f.toPath(), currentDir);
     System.out.println("dkhalt");
     } catch (IOException ex) {
     System.out.println(ex);
     }
     }*/

    @FXML
    private void Allerliste(ActionEvent event) {
        Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ListePublication.fxml")), 765, 573);
        } catch (IOException ex) {
            Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void publierAction(MouseEvent event) {
        
      PublicationService ps=new PublicationService();
        //System.out.println(TextA.getText()+outputfile.getAbsolutePath());
      ps.ajouterPublication(TextA.getText(), TextF.getText(),outputfile.getName());
    }
}
