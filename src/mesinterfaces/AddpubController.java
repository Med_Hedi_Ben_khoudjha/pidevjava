/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinterfaces;


import Services.PublicationService;
import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import static mesinterfaces.ModifierPublicationController.in;
import static pidev.main.stage;

/**
 * FXML Controller class
 *
 * @author MED
 */
public class AddpubController implements Initializable {

    @FXML
    private AnchorPane ap;
    @FXML
    private TextArea TextA;
    @FXML
    private TextField TextF;
    @FXML
    private JFXButton publierButton;
    @FXML
    private JFXButton Listepub;

    static JFXButton file;
    
    File outputfile;
    File f;
    @FXML
    private ImageView publier;


 final FileChooser fileChooser = new FileChooser();
 static File fil;
    @FXML
    private FontAwesomeIcon imageChooser;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imageChooser.setOnMouseClicked(new EventHandler<Event>() {

            public void handle(Event event) {
                configureFileChooser(fileChooser);
                fil = fileChooser.showOpenDialog((Stage) ap.getScene().getWindow());
                if (file != null) {
                    try {
                        BufferedImage in = ImageIO.read(fil);
                        outputfile = new File("src/image/", fil.getName());
                        ImageIO.write(in, "png", outputfile);

                    } catch (IOException ex) {
                        Logger.getLogger(AddPublicationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                    //setFile(file);

            }
        });
        publier.setOnMouseClicked((Event event) -> {
            //configureFileChooser(fileChooser);
            fil = fileChooser.showOpenDialog((Stage) ap.getScene().getWindow());
            if (fil != null) //setFile(file);
            {
                System.out.println(fil.getName());
            }
        });    
    }    
     Stage stage;
    Scene scene;

    @FXML
    private void publierAction(MouseEvent event) {
        
        
      PublicationService ps=new PublicationService();
        //System.out.println(TextA.getText()+outputfile.getAbsolutePath());
      ps.ajouterPublication(TextA.getText(), TextF.getText(),outputfile.getName());
      
    }
    
    @FXML
    private void allerlist(MouseEvent event) {
        
         Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ListePublication.fxml")), 765, 573);
        } catch (IOException ex) {
            Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }
     private static void configureFileChooser(final FileChooser fileChooser) {
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

    }
}
