/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinterfaces;

import Services.PublicationService;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import entities.Publication;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
//import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
//import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import utils.BadWords;

/**
 * FXML Controller class
 *
 * @author Malek
 */
public class ModifierPublicationController implements Initializable {

    @FXML
    private AnchorPane ap;
    @FXML
    private TextArea contenu;
    @FXML
    private FontAwesomeIconView imageChooser;
    @FXML
    private Button modifButton;
    @FXML
    private Button annulerButton;

    @FXML
    private ImageView imageView;

    public static Publication pub;

    static File outputfile;
    final FileChooser fileChooser = new FileChooser();
    static BufferedImage in;
    File f;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        contenu.setText(pub.getContenu());
        File file = new File("src/image/" + pub.getImage());
        //System.out.println("src/image/"+pub.getImage());
        Image image = new Image(file.toURI().toString());
        if (image.isError()) {
            System.out.println(file.getAbsoluteFile());
        }
        imageView.setImage(image);
        imageChooser.setOnMouseClicked((Event event) -> {
            configureFileChooser(fileChooser);
            f = fileChooser.showOpenDialog((Stage) ap.getScene().getWindow());
            if (f != null) {
                try {
                    in = ImageIO.read(f);
                    outputfile = new File("src/image/", f.getName());
                    Image image1 = new Image(f.toURI().toString());
                    imageView.setImage(image1);
                } catch (IOException ex) {
                    Logger.getLogger(AddPublicationController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //setFile(file);
        });

    }

    @FXML
    private void modifierPub(ActionEvent event) {
        try {
            PublicationService ps = new PublicationService();
            BadWords.loadConfigs();
            if (f != null) {
                if (BadWords.filterText(contenu.getText())) {
                    
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("gros mot");
                    alert.setHeaderText("gros mot trouve");
                    alert.setContentText("cette application n'autorise pas ces termes ");
                    alert.showAndWait();
                } else {
                    ps.modifierPublication(pub, contenu.getText(), f.getName());
                }
                ImageIO.write(in, "png", outputfile);

            } else {
                if (BadWords.filterText(contenu.getText())) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("gros mot");
                    alert.setHeaderText("gros mot trouve");
                    alert.setContentText("cette application n'autorise pas ces termes ");
                    alert.showAndWait();
                } else {
                    ps.modifierPublication(pub, contenu.getText(), pub.getImage());
                }
            }
            Node node = (Node) event.getSource();
            stage = (Stage) node.getScene().getWindow();

            try {
                scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/listePublication.fxml")), 765, 573);
            } catch (IOException ex) {
                Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            stage.setScene(scene);
            stage.show();

        } catch (Exception ex) {
            Logger.getLogger(ModifierPublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Stage stage;
    Scene scene;

    @FXML
    private void Annuler(ActionEvent event) {

        Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/listePublication.fxml")), 765, 573);
        } catch (IOException ex) {
            Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }

    private static void configureFileChooser(
            final FileChooser fileChooser) {
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

    }
}
