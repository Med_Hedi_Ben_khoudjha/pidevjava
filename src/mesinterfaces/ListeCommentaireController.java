/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinterfaces;

import Crud.CommentaireCrud;
import Crud.PublicationCrud;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import entities.Commentaire;
import entities.Publication;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Malek
 */
public class ListeCommentaireController implements Initializable {
    @FXML
    private AnchorPane Commentaire;
    @FXML
    private TableView<Commentaire> ComTableView;
    @FXML
    private TableColumn<Commentaire, String> Membre;
    @FXML
    private TableColumn<Commentaire, String> Date;
    @FXML
    private TableColumn<Commentaire, String> Contenu;
    @FXML
    private Button ModifierC;
    @FXML
    private Button SupprimerC;
    private AnchorPane myPane;
    
    Stage stage;
    Scene scene;
    @FXML
    private Button Listepub;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         myPane = new AnchorPane();
        //JFXTreeTableView<Commentaire> treeview = new JFXTreeTableView<Commentaire>();
       // myPane.getChildren().add(treeview);
        ComTableView.setVisible(true);
        CommentaireCrud ts = new CommentaireCrud();
        List<Commentaire> myList = ts.afficherCommentaire();
        ObservableList<Commentaire> myTransport = FXCollections.observableArrayList();
        System.out.println(myList);
         
        Membre.setCellValueFactory(new PropertyValueFactory<>("membre"));
        Date.setCellValueFactory(new PropertyValueFactory<>("date"));
        Contenu.setCellValueFactory(new PropertyValueFactory<>("message"));
        
        
        myList.forEach(e -> myTransport.add(e));
        System.out.println(myTransport);
        

            
        ComTableView.setItems(myTransport);
        // TODO
    }    

    @FXML
    private void ModifierCom(ActionEvent event) {
    }

    @FXML
    private void SupprimerCom(ActionEvent event) {
        Commentaire com = ComTableView.getSelectionModel().getSelectedItem();
       System.out.println(com.getId()); 
         CommentaireCrud pcrud= new CommentaireCrud();
         pcrud.supprimerCommentaire(com.getId());
        ObservableList<Commentaire> s=  FXCollections.observableArrayList(pcrud.afficherCommentaire());
      ComTableView.setItems(s);
    }

    private void Raffraichir(ActionEvent event) {
         Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ListeCommentaire.fxml")), 686, 495);
        } catch (IOException ex) {
            Logger.getLogger(ListeCommentaireController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void Allerliste(ActionEvent event) {
         Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ListePublication.fxml")), 772, 573);
        } catch (IOException ex) {
            Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }
    
}
