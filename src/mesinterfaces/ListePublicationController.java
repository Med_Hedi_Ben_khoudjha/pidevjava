/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinterfaces;

import Crud.PublicationCrud;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
//import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import entities.Publication;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Malek
 */
public class ListePublicationController implements Initializable {

    @FXML
    private AnchorPane Publication;
    @FXML
    private TableColumn<Publication, String> Membre = new TableColumn<>();
    @FXML
    private TableColumn<Publication, String> Date = new TableColumn<>();
    @FXML
    private TableColumn<Publication, String> Contenu = new TableColumn<>();
    @FXML
    private Button ModifierP;
    @FXML
    private Button SupprimerP;
    @FXML
    private Button ListerC;
    @FXML
    private Button AjouterC;
    @FXML
    private TableView<Publication> PubTableView;
    @FXML
    private AnchorPane myPane;
    @FXML
    private Button Raffraichir;
    Stage stage;
    Scene scene;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        myPane = new AnchorPane();
        JFXTreeTableView<Publication> treeview = new JFXTreeTableView<Publication>();
        myPane.getChildren().add(treeview);
        PubTableView.setVisible(true);
        PublicationCrud ts = new PublicationCrud();
        List<Publication> myList = ts.afficherPublication();
        ObservableList<Publication> myTransport = FXCollections.observableArrayList();
        System.out.println(myList);
      //  Membre.setCellValueFactory(new ProprertyValueFactory<>("nom"));
         
        Membre.setCellValueFactory(new PropertyValueFactory<>("membre"));
        Date.setCellValueFactory(new PropertyValueFactory<>("date"));
        Contenu.setCellValueFactory(new PropertyValueFactory<>("contenu"));
        
        

       

        
        

        myList.forEach(e -> myTransport.add(e));
        System.out.println(myTransport);
        final TreeItem<Publication> root = new RecursiveTreeItem<>(myTransport, RecursiveTreeObject::getChildren);
        treeview = new JFXTreeTableView<>(root);
        //treeview.getColumns().setAll(Membre, date, contenu);
        treeview.setRoot(root);
        treeview.setShowRoot(false);
        // treeview.setLayoutX(14);
        //treeview.setLayoutY(105);
        //myPane.getChildren().add(treeview);
        

            
        PubTableView.setItems(myTransport);

        
    }

    @FXML
    private void ModifierPubl(ActionEvent event) {
         
        Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();
        
        try {
            ModifierPublicationController.pub=PubTableView.getSelectionModel().getSelectedItem();
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ModifierPublication.fxml")), 765, 573);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("");
                    alert.setHeaderText("vous devez selectionner la publication");
                    alert.setContentText("vous devez selectionner la publication");
                    alert.showAndWait();
        }
        
       
        
    }

    @FXML
    private void SupprimerPubl(ActionEvent event) {
        Publication pub = PubTableView.getSelectionModel().getSelectedItem();
        System.out.println(pub.getId());
        PublicationCrud pcrud= new PublicationCrud();
        pcrud.supprimerPublication(pub.getId());
        ObservableList<Publication> s=FXCollections.observableArrayList(pcrud.afficherPublication()) ;
        PubTableView.setItems(s);
        
    }

    @FXML
    private void ListerCom(ActionEvent event) {
        Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ListeCommentaire.fxml")), 726, 495);
        } catch (IOException ex) {
            Logger.getLogger(CommentaireController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void AjouterComm(ActionEvent event) {
       
      
           Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
             Publication pub = PubTableView.getSelectionModel().getSelectedItem();
             System.out.println(pub.getId()); 
             scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/Commentaire.fxml")), 600, 400);
               stage.setScene(scene);
               stage.show();
        } catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("");
                    alert.setHeaderText("vous devez selectionner la publication");
                    alert.setContentText("vous devez selectionner la publication");
                    alert.showAndWait();
        }
      
    }

    @FXML
    private void Raffraichir(ActionEvent event) {
        Node node = (Node) event.getSource();
        stage = (Stage) node.getScene().getWindow();

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("../mesinterfaces/ListePublication.fxml")), 765, 573);
        } catch (IOException ex) {
            Logger.getLogger(ListePublicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setScene(scene);
        stage.show();
    }

}
